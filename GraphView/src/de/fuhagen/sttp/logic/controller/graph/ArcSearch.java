package de.fuhagen.sttp.logic.controller.graph;

import java.awt.Point;
import java.util.Vector;

import de.fuhagen.sttp.data.graph.Arc;
import de.fuhagen.sttp.util.DoublePoint;
import de.fuhagen.sttp.util.MathUtils;

/**
 * This class contains methods to search for graph arcs and graph arc drag points.
 * 
 * @author thomas
 *
 */
public final class ArcSearch {

    /**
     * The allowed offset for mouse arc detection.
     */
    private static final int MOUSE_DELTA      = 5;
    /**
     * The allowed offset for mouse drag point detection.
     */
    private static final int DRAG_POINT_DELTA = 10;

    /**
     * Utility class.
     */
    private ArcSearch() {
    }

    /**
     * This method search an arc of which is next to a given point.
     * 
     * 
     * @param arcs
     *      {@link Vector} of {@link Arc}s to search
     * @param mousePoint
     *      {@link Point} to search
     * @return
     *      arc if found or null
     */
    public static Arc findArc(final Vector<Arc> arcs, final Point mousePoint) {
        Arc clickedArc = null;
        for (Arc arc : arcs) {
            if (isArcClicked(arc, mousePoint)) {
                clickedArc = arc;
                break;
            }
        }
        return clickedArc;
    }

    /**
     * This method tests if the given point is next to the given arc
     * for straight arcs.
     * 
     * @param arc
     *      arc as {@link Arc}
     * @param mousePoint
     *      mouse point as {@link Point}
     * @return
     *      true if point is next to arc, false else
     */
    private static boolean isArcClicked(final Arc arc, final Point mousePoint) {
        boolean isClicked = false;

        Point start = new Point(arc.getStart().getX(), arc.getStart().getY());
        Point end = new Point(arc.getEnd().getX(), arc.getEnd().getY());

        int minX = 0;
        int maxX = 0;
        if (start.x <= end.x) {
            minX = start.x;
            maxX = end.x;
        } else {
            minX = end.x;
            maxX = start.x;
        }

        int minY = 0;
        int maxY = 0;
        if (start.y <= end.y) {
            minY = start.y;
            maxY = end.y;
        } else {
            minY = end.y;
            maxY = start.y;
        }

        for (Point dragPoint : arc.getDragPoints()) {
            if (dragPoint.x < minX) {
                minX = dragPoint.x;
            }
            if (dragPoint.x > maxX) {
                maxX = dragPoint.x;
            }
            if (dragPoint.y < minY) {
                minY = dragPoint.y;
            }
            if (dragPoint.y > maxY) {
                maxY = dragPoint.y;
            }
        }

        minX -= MOUSE_DELTA;
        maxX += MOUSE_DELTA;
        minY -= MOUSE_DELTA;
        maxY += MOUSE_DELTA;

        if (minX <= mousePoint.getX() && maxX >= mousePoint.getX()) {
            if (minY <= mousePoint.getY() && maxY >= mousePoint.getY()) {

                Vector<Point> arcPoints = new Vector<Point>();
                arcPoints.addAll(arc.getDragPoints());
                arcPoints.insertElementAt(start, 0);
                arcPoints.add(end);

                for (int i = 0; i < arcPoints.size() - 1; i++) {
                    double delta = calcDistance(arcPoints.get(i), arcPoints
                            .get(i + 1), mousePoint);
                    if (Math.abs(delta) <= MOUSE_DELTA) {
                        isClicked = true;
                        break;
                    }
                }
            }
        }
        return isClicked;
    }

    /**
     * This method calculates the distance of the point mousePoint of 
     * the straight line through start and end.
     * 
     * @param start
     *      first point of the straight line as {@link Point}
     * @param end
     *      second point of the straight line as {@link Point} 
     * @param mousePoint
     *      mouse position as {@link Point}
     * @return
     *      distance of the straight line
     */
    private static double calcDistance(final Point start, final Point end,
            final Point mousePoint) {
        // Direction vector
        DoublePoint direction = new DoublePoint(end.x - start.x, end.y
                - start.y);
        // calculate normal vector for Hesse Normal Form
        DoublePoint dir90 = MathUtils.rotateVector(direction, Math.PI / 2);
        // normal vector of the straight line
        DoublePoint norm = MathUtils.sizeVector(dir90, 1.0);
        // calculate distance of straight line from origin for Hesse Normal Form
        double dist = norm.getX() * start.x + norm.getY() * start.y;

        //use Hesse Normal Form to calculate distance of point from straight line
        double delta = norm.getX() * mousePoint.x + norm.getY() * mousePoint.y
                - dist;
        return delta;
    }

    /**
     * This method search the drag point of the given arc next to 
     * the mouse position.
     * 
     * @param arc
     *      arc as {@link Arc}
     * @param mousePoint
     *      mouse point as {@link Point}
     * @return
     *      found drag point as {@link Point} or null
     */
    public static Point findDragPoint(final Arc arc, final Point mousePoint) {
        Point foundPoint = null;
        for (Point dragPoint : arc.getDragPoints()) {
            int dx = dragPoint.x - mousePoint.x;
            int dy = dragPoint.y - mousePoint.y;

            int sqLen = dx * dx + dy * dy;
            if (sqLen <= DRAG_POINT_DELTA * DRAG_POINT_DELTA) {
                foundPoint = dragPoint;
                break;
            }
        }
        return foundPoint;
    }

    /**
     * This method calculates the right segment for a new drag point on the given
     * position. If no right position is found the drag point will be added at
     * the end of the first segment of the arc.
     * 
     * 
     * @param arc
     *      arc as {@link Arc}
     * @param mousePoint
     *      mouse point as {@link Point}
     * @return
     *      segment for new drag point
     */
    public static int calcDragPointOrder(final Arc arc, final Point mousePoint) {
        int segment = 0;

        Vector<Point> arcPoints = new Vector<Point>();
        arcPoints.addAll(arc.getDragPoints());
        arcPoints.insertElementAt(arc.getStart().getPoint(), 0);
        arcPoints.add(arc.getEnd().getPoint());

        for (; segment < arcPoints.size() - 1; segment++) {
            double distance = calcDistance(arcPoints.get(segment), arcPoints
                    .get(segment + 1), mousePoint);
            if (Math.abs(distance) <= MOUSE_DELTA) {
                break;
            }
        }
        return segment;
    }

}
