package de.fuhagen.sttp.test.logic.controller.labeledgraph;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;


@RunWith(Suite.class)
@SuiteClasses({LabeledGraphControllerTest.class})
public class LogicControllerLabeledgraphTests {

}
