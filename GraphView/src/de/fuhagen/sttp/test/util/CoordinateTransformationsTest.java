package de.fuhagen.sttp.test.util;

import static org.junit.Assert.assertTrue;

import java.awt.Dimension;
import java.awt.Point;

import org.junit.Test;

import de.fuhagen.sttp.util.CoordinateTransformations;
import de.fuhagen.sttp.util.DoublePoint;
import de.fuhagen.sttp.util.DoubleRectangle;

/**
 * This class test the {@link CoordinateTransformations} utility methods.
 * 
 * @author thomas
 *
 */
public final class CoordinateTransformationsTest {

    /**
     * Translate X value for tests.
     */
    private static final double TRANSLATE_X = 100.0;
    /**
     * Translate Y value for tests.
     */
    private static final double TRANSLATE_Y = -200.5;
    /**
     * Scale X value for tests.
     */
    private static final double SCALE_X     = 2.0;
    /**
     * Scale Y value for tests.
     */
    private static final double SCALE_Y     = 0.5;

    /**
     * X coordinate of test point.
     */
    private static final int    POINT_X     = -300;
    /**
     * Y coordinate of test point.
     */
    private static final int    POINT_Y     = 500;
    /**
     * Width of test area.
     */
    private static final int    WIDTH       = 200;
    /**
     * Height of test area.
     */
    private static final int    HEIGHT      = 200;

    /**
     * This method tests the calculation of the visible area.
     */
    @Test
    public void testTransformScreenToGraph() {
        double xLeftTop = -TRANSLATE_X / SCALE_X;
        double yLeftTop = -TRANSLATE_Y / SCALE_Y;
        double visibleWidth = WIDTH / SCALE_X;
        double visibleHeight = HEIGHT / SCALE_Y;

        DoubleRectangle visibleArea = CoordinateTransformations
                .transformScreenToGraph(new DoublePoint(TRANSLATE_X,
                        TRANSLATE_Y), SCALE_X, SCALE_Y, new Dimension(WIDTH,
                        HEIGHT));
        assertTrue("Upper left x: " + xLeftTop + " == " + visibleArea.getX()
                + ".", xLeftTop == visibleArea.getX());
        assertTrue("Upper left y." + yLeftTop + " == " + visibleArea.getY()
                + ".", yLeftTop == visibleArea.getY());
        assertTrue("Visible width: " + visibleWidth + " == "
                + visibleArea.getWidth(), visibleWidth == visibleArea
                .getWidth());
        assertTrue("Visible height." + visibleHeight + " == "
                + visibleArea.getHeight(), visibleHeight == visibleArea
                .getHeight());
    }

    /**
     * This method tests the transformation form screen points to graph points.
     */
    @Test
    public void testTransformPointToScreen() {
        double x = (POINT_X * SCALE_X) + TRANSLATE_X;
        double y = (POINT_Y * SCALE_Y) + TRANSLATE_Y;
        DoublePoint tranlated = CoordinateTransformations
                .transformGraphPointToScreen(new DoublePoint(TRANSLATE_X,
                        TRANSLATE_Y), SCALE_X, SCALE_Y, new Point(POINT_X,
                        POINT_Y));
        assertTrue("Screen to graph, x.", x == tranlated.getX());
        assertTrue("Screen to graph, y.", y == tranlated.getY());
    }

    /**
     * This method tests the transformation form graph points to screen points.
     */
    @Test
    public void testTransformPointToGraph() {
        double x = (POINT_X - TRANSLATE_X) / SCALE_X;
        double y = (POINT_Y - TRANSLATE_Y) / SCALE_Y;

        DoublePoint translated = CoordinateTransformations
                .transformScreenPointToGraph(new DoublePoint(TRANSLATE_X,
                        TRANSLATE_Y), SCALE_X, SCALE_Y, new Point(POINT_X,
                        POINT_Y));

        assertTrue("Graph to screen, x: " + translated.getX() + " == " + x
                + ".", x == translated.getX());
        assertTrue("Graph to screen, y: " + translated.getY() + " == " + y
                + ".", y == translated.getY());
    }

}
