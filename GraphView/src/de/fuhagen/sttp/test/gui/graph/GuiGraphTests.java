package de.fuhagen.sttp.test.gui.graph;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({GraphViewTest.class, GuiArcTest.class, GuiNodeTest.class})
public class GuiGraphTests {

}
