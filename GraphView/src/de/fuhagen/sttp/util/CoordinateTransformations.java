package de.fuhagen.sttp.util;

import java.awt.Dimension;
import java.awt.Point;
import java.awt.geom.AffineTransform;
import java.awt.geom.NoninvertibleTransformException;
import java.awt.geom.Point2D;

/**
 * This utility class groups help methods for graph to screen and screen to graph
 * coordinate and size transformations.
 */
public final class CoordinateTransformations {

    /**
     * This is a utility class.
     */
    private CoordinateTransformations() {
    }

    /**
     * This method translates the screen view rectangle to its graph rectangle.
     * 
     * @param translate
     *      translation of viewport
     * @param zoomX
     *      x zoom factor
     * @param zoomY
     *      y zoom factor
     * @param screen
     *      size of the screen view as {@link Dimension}
     * @return
     *      visible graph rectangle
     */
    public static DoubleRectangle transformScreenToGraph(
            final DoublePoint translate, final double zoomX,
            final double zoomY, final Dimension screen) {
        AffineTransform transform = new AffineTransform(zoomX, 0, 0, zoomY,
                translate.getX(), translate.getY());

        DoubleRectangle rectangle = null;
        try {
            transform.invert();

            Point2D upLeft = transform.transform(new Point(0, 0), null);
            Point2D bottomRight = transform.transform(new Point(screen.width,
                    screen.height), null);

            rectangle = new DoubleRectangle(upLeft.getX(), upLeft.getY(),
                    bottomRight.getX() - upLeft.getX(), bottomRight.getY()
                            - upLeft.getY());
        } catch (NoninvertibleTransformException e) {
            System.err.println(e.getMessage());
        }

        return rectangle;
    }

    /**
     * This method translates a graph point to a screen point.
     * 
     * @param translate
     *      view translation
     * @param zoomX
     *      x zoom
     * @param zoomY
     *      y zoom
     * @param point
     *      point to translate
     * @return
     *      translated point as {@link DoublePoint}
     */
    public static DoublePoint transformScreenPointToGraph(
            final DoublePoint translate, final double zoomX,
            final double zoomY, final Point point) {
        AffineTransform transform = new AffineTransform(zoomX, 0, 0, zoomY,
                translate.getX(), translate.getY());

        DoublePoint transformed = null;
        try {
            transform.invert();

            Point2D transformed2d = transform.transform(point, null);
            transformed = new DoublePoint(transformed2d.getX(), transformed2d
                    .getY());

        } catch (NoninvertibleTransformException e) {
            System.err.println(e.getMessage());
        }

        return transformed;
    }

    /**
     * This method translates a screen point to a graph point.
     * 
     * @param translate
     *         view translation
     * @param zoomX
     *      x zoom factor
     * @param zoomY
     *      y zoom factor
     * @param point
     *      point to translate
     * @return
     *      translated point as {@link DoublePoint}
     */
    public static DoublePoint transformGraphPointToScreen(
            final DoublePoint translate, final double zoomX,
            final double zoomY, final Point point) {
        AffineTransform transform = new AffineTransform(zoomX, 0, 0, zoomY,
                translate.getX(), translate.getY());

        Point2D transformedPoint = transform.transform(point, null);
        return new DoublePoint(transformedPoint.getX(), transformedPoint.getY());
    }
}
