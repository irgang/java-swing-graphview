/**
 * 
 */
package de.fuhagen.sttp.test.util;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import de.fuhagen.sttp.util.DoubleRectangle;

/**
 * @author thomas
 *
 */
public class DoubleRectangleTest {

    /**
     * Test method for {@link de.fuhagen.sttp.util.DoubleRectangle#getX()}.
     */
    @Test
    public void testGetX() {
        DoubleRectangle r = new DoubleRectangle(1.23, 4.56, 7.8, 9.11);
        assertTrue(r.getX() == 1.23);
    }

    /**
     * Test method for {@link de.fuhagen.sttp.util.DoubleRectangle#getY()}.
     */
    @Test
    public void testGetY() {
        DoubleRectangle r = new DoubleRectangle(1.23, 4.56, 7.8, 9.11);
        assertTrue(r.getY() == 4.56);
    }

    /**
     * Test method for {@link de.fuhagen.sttp.util.DoubleRectangle#getWidth()}.
     */
    @Test
    public void testGetWidth() {
        DoubleRectangle r = new DoubleRectangle(1.23, 4.56, 7.8, 9.11);
        assertTrue(r.getWidth() == 7.8);
    }

    /**
     * Test method for {@link de.fuhagen.sttp.util.DoubleRectangle#getHeight()}.
     */
    @Test
    public void testGetHeight() {
        DoubleRectangle r = new DoubleRectangle(1.23, 4.56, 7.8, 9.11);
        assertTrue(r.getHeight() == 9.11);
    }

}
