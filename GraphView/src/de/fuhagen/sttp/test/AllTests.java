package de.fuhagen.sttp.test;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import de.fuhagen.sttp.test.data.DataTests;
import de.fuhagen.sttp.test.gui.graph.GuiGraphTests;
import de.fuhagen.sttp.test.logic.controller.LogicControllerTests;
import de.fuhagen.sttp.test.util.UtilTests;

@RunWith(Suite.class)
@SuiteClasses({UtilTests.class, LogicControllerTests.class,
        GuiGraphTests.class, DataTests.class})
public class AllTests {

}
