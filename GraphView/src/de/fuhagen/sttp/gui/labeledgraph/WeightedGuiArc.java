package de.fuhagen.sttp.gui.labeledgraph;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Point;
import java.util.Vector;

import de.fuhagen.sttp.data.labeledgraph.WeightedArc;
import de.fuhagen.sttp.gui.graph.GuiArc;
import de.fuhagen.sttp.logic.controller.graph.GraphController;
import de.fuhagen.sttp.util.DoublePoint;

public class WeightedGuiArc extends GuiArc {

    private WeightedArc weightedArc;

    public WeightedGuiArc(WeightedArc data, GraphController gc) {
        super(data, gc);
        weightedArc = data;
    }

    public void drawArc(final Graphics2D g2d) {
        super.drawArc(g2d);

        Vector<Point> points = new Vector<Point>();
        points.add(calcStartPoint(weightedArc));
        points.addAll(weightedArc.getDragPoints());
        points.add(calcEndPoint(weightedArc));

        int middle = points.size() / 2;
        Point start = points.get(middle - 1);
        Point end = points.get(middle);

        Point vect = new Point((end.x - start.x) / 2, (end.y - start.y) / 2);
        Point anchor = new Point(start.x + vect.x, start.y + vect.y);

        int weight = weightedArc.getWeight();
        if (weight != 1) {
            DoublePoint offset = weightedArc.getOffset();
            int x = anchor.x + (int) offset.getX();
            int y = anchor.y + (int) offset.getY();

            Font font = g2d.getFont();
            y += g2d.getFontMetrics(font).getAscent();

            int width = g2d.getFontMetrics(font).stringWidth(weight + "");
            x -= width / 2;

            g2d.setColor(Color.GRAY);

            g2d.drawString(weight + "", x, y);
        }

    }

}
