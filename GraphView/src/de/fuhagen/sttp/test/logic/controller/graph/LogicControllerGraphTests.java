package de.fuhagen.sttp.test.logic.controller.graph;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;


@RunWith(Suite.class)
@SuiteClasses({ArcSearchTest.class, GraphControllerTest.class,
        NodeSearchTest.class})
public class LogicControllerGraphTests {

}
