/**
 * This package contains utility classes.
 * 
 * @author thomas
 *
 */
package de.fuhagen.sttp.util;