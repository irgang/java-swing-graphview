package de.fuhagen.sttp.demo;

import java.awt.BorderLayout;

import javax.swing.JFrame;
import javax.swing.JLabel;

import de.fuhagen.sttp.data.graph.Node;
import de.fuhagen.sttp.gui.graph.GraphView;
import de.fuhagen.sttp.logic.controller.labeledgraph.LabeledGraphController;

/**
 * Demo for {@link GraphPanel} and {@link GraphNode}s.
 * 
 * @author thomas
 *
 */
public class LabeledGraphViewDemo extends JFrame {

    /**
     * Serial version UID.
     */
    private static final long serialVersionUID = -5247430927670011690L;

    /**
     * This method runs the demo.
     * 
     * @param args
     */
    public static void main(String[] args) {
        new LabeledGraphViewDemo();
    }

    /**
     * This constructor builds a new {@link JFrame} which contains a {@link GraphView}.
     */
    public LabeledGraphViewDemo() {
        super("Labeled graph view demo");

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLayout(new BorderLayout());

        LabeledGraphController controller = new LabeledGraphController();
        GraphView graph = controller.createView(true);
        add(graph, BorderLayout.CENTER);

        JLabel hallo = new JLabel("Hallo");
        JLabel welt = new JLabel("Welt");
        graph.add(hallo);
        graph.add(welt);
        hallo.setBounds(70, 90, 5, 5);
        welt.setBounds(210, 280, 5, 5);

        Node a = controller.createLabeledNode(200, 100, "a");
        Node b = controller.createLabeledNode(100, 200, "b");
        Node c = controller.createLabeledNode(500, 300, "c");

        controller.createWeightedArc(a, b, 1);
        controller.createWeightedArc(b, c, 2);
        controller.createWeightedArc(c, a, 3);

        pack();
        setVisible(true);
    }
}
