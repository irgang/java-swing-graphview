package de.fuhagen.sttp.test.logic.controller.graph;

import static org.junit.Assert.assertTrue;

import java.awt.Point;
import java.util.Vector;

import org.junit.Test;

import de.fuhagen.sttp.data.graph.Node;
import de.fuhagen.sttp.logic.controller.graph.NodeSearch;

/**
 * Tests for {@link NodeSearch}.
 * 
 * @author thomas
 *
 */
public class NodeSearchTest {

    /**
     * X coordinate of the first test node.
     */
    private static final int FIRST_NODE_X  = -500;
    /**
     * Y coordinate of the first test node.
     */
    private static final int FIRST_NODE_Y  = -500;
    /**
     * X coordinate of the second test node.
     */
    private static final int SECOND_NODE_X = 500;
    /**
     * Y coordinate of the second test node.
     */
    private static final int SECOND_NODE_Y = 500;
    /**
     * Offset for tests.
     */
    private static final int DELTA         = 5;
    /**
     * X coordinate of no node.
     */
    private static final int NO_NODE_X     = 800;
    /**
     * Y coordinate of no node.
     */
    private static final int NO_NODE_Y     = -1000;

    /**
     * This test tries to find a node with negative coordinates, a node with 
     * positive coordinates and a test which is inside of no node. 
     */
    @Test
    public final void testFindNode() {
        Vector<Node> nodes = new Vector<Node>();
        Node node = new Node(FIRST_NODE_X, FIRST_NODE_Y);
        Node node2 = new Node(SECOND_NODE_X, SECOND_NODE_Y);
        nodes.add(node);
        nodes.add(node2);

        Node foundNode = NodeSearch.findNode(nodes, new Point(FIRST_NODE_X
                + DELTA, FIRST_NODE_Y + DELTA));
        assertTrue("Node with negative coordinates", foundNode == node);

        foundNode = NodeSearch.findNode(nodes, new Point(SECOND_NODE_X - DELTA,
                SECOND_NODE_Y - DELTA));
        assertTrue("Node with positive coordinates", foundNode == node2);

        foundNode = NodeSearch.findNode(nodes, new Point(NO_NODE_X, NO_NODE_Y));
        assertTrue("No node found.", foundNode == null);
    }
}
