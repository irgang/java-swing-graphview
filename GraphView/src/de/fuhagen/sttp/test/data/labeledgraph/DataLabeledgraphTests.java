package de.fuhagen.sttp.test.data.labeledgraph;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;


@RunWith(Suite.class)
@SuiteClasses({LabeledNodeTest.class, WeightedArcTest.class})
public class DataLabeledgraphTests {

}
