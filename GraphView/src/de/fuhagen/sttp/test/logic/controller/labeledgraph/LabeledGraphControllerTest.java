package de.fuhagen.sttp.test.logic.controller.labeledgraph;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import de.fuhagen.sttp.data.labeledgraph.LabeledNode;
import de.fuhagen.sttp.data.labeledgraph.WeightedArc;
import de.fuhagen.sttp.logic.controller.labeledgraph.LabeledGraphController;

public class LabeledGraphControllerTest {

    @Test
    public void testCreateLabeledNode() {
        LabeledGraphController controller = new LabeledGraphController();
        LabeledNode node = controller.createLabeledNode(100, 200, "Hallo");

        assertTrue(node.getX() == 100);
        assertTrue(node.getY() == 200);
        assertTrue(node.getLabel().equals("Hallo"));
    }

    @Test
    public void testCreateWeightedArc() {
        LabeledGraphController controller = new LabeledGraphController();
        LabeledNode start = controller.createLabeledNode(100, 200, "Hallo");
        LabeledNode end = controller.createLabeledNode(300, 400, "Welt");
        WeightedArc arc = controller.createWeightedArc(start, end, 2);

        assertTrue(arc.getStart() == start);
        assertTrue(arc.getEnd() == end);
        assertTrue(arc.getWeight() == 2);
    }
}
