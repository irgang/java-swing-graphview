/**
 * 
 */
package de.fuhagen.sttp.test.util;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import de.fuhagen.sttp.util.DoublePoint;

/**
 * @author thomas
 *
 */
public class DoublePointTest {

    /**
     * Test method for {@link de.fuhagen.sttp.util.DoublePoint#getX()}.
     */
    @Test
    public void testGetX() {
        DoublePoint p = new DoublePoint(1.23, 4.56);
        assertTrue(p.getX() == 1.23);
    }

    /**
     * Test method for {@link de.fuhagen.sttp.util.DoublePoint#getY()}.
     */
    @Test
    public void testGetY() {
        DoublePoint p = new DoublePoint(1.23, 4.56);
        assertTrue(p.getY() == 4.56);
    }

}
