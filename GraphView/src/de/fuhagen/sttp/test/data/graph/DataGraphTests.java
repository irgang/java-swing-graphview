package de.fuhagen.sttp.test.data.graph;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;


@RunWith(Suite.class)
@SuiteClasses({ArcTest.class, NodeTest.class})
public class DataGraphTests {

}
