package de.fuhagen.sttp.logic.controller.graph;

import java.awt.Point;
import java.util.Vector;

import de.fuhagen.sttp.data.graph.Node;

/**
 * This class contains methods to search the node which contains a point.
 * 
 * @author thomas
 *
 */
public final class NodeSearch {

    /**
     * Utility class.
     */
    private NodeSearch() {
    }

    /**
     * This method search the first node which contains the given point.
     * 
     * @param nodes
     *      nodes as {@link Vector} of {@link Node}s
     * @param mousePoint
     *      point as {@link Point}
     * @return
     *      node which contains point or null
     */
    public static Node findNode(final Vector<Node> nodes,
            final Point mousePoint) {
        Node clickedNode = null;
        for (Node node : nodes) {
            if (isNodeClicked(node, mousePoint)) {
                clickedNode = node;
                break;
            }
        }
        return clickedNode;
    }

    /**
     * This method tests if a given point inside of a given node.
     * 
     * @param node
     *      node as {@link Node}
     * @param mousePoint
     *      point as {@link Point}
     * @return
     *      true if point is inside of node, false else
     */
    private static boolean isNodeClicked(final Node node,
            final Point mousePoint) {
        boolean isClicked = false;
        if (node.getX() - (node.getWidth() / 2) <= mousePoint.getX()
                && node.getX() + (node.getWidth() / 2) >= mousePoint.getX()) {
            if (node.getY() - (node.getHeight() / 2) <= mousePoint.getY()
                    && node.getY() + (node.getHeight() / 2) >= mousePoint
                            .getY()) {
                isClicked = true;
            }
        }
        return isClicked;
    }

}
