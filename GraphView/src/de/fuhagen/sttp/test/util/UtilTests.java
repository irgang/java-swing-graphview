package de.fuhagen.sttp.test.util;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;


@RunWith(Suite.class)
@SuiteClasses({CoordinateTransformationsTest.class, DoublePointTest.class,
        DoubleRectangleTest.class, MathUtilsTest.class})
public class UtilTests {

}
