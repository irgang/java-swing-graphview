package de.fuhagen.sttp.data.labeledgraph;

import de.fuhagen.sttp.data.graph.Node;
import de.fuhagen.sttp.util.DoublePoint;

public class LabeledNode extends Node {

    private static final int LABEL_OFFSET = 5;

    private String           label;
    private DoublePoint      offset;

    public LabeledNode(final int centerX, final int centerY) {
        super(centerX, centerY);
        label = null;

        offset = new DoublePoint(0, (getAbsolutHeight() / 2) + LABEL_OFFSET);
    }

    public LabeledNode(final int centerX, final int centerY, String nodeLabel) {
        this(centerX, centerY);
        label = nodeLabel;
    }

    /**
     * @return the label
     */
    public String getLabel() {
        return label;
    }

    /**
     * @param label the label to set
     */
    public void setLabel(String newlabel) {
        label = newlabel;
    }

    /**
     * @return the offset
     */
    public DoublePoint getOffset() {
        return offset;
    }
    
    /**
     * @param offset the offset to set
     */
    public void setOffset(DoublePoint offset) {
        this.offset = offset;
    }

    @Override
    public String toString() {
        return "LabeledNode at (" + getX() + ", " + getY() + ")";
    }

}
