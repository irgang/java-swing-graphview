package de.fuhagen.sttp.gui.graph;

import java.awt.BasicStroke;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;

import de.fuhagen.sttp.data.graph.Node;
import de.fuhagen.sttp.util.DoublePoint;
import de.fuhagen.sttp.util.MathUtils;

public class GuiNode {

    private Node node;

    public GuiNode(Node data) {
        super();

        node = data;
    }

    /**
     * This method draws a node.
     * 
     * @param node
     *          {@link Node} to draw
     * @param g2d
     *          graphics context
     */
    public void drawNode(final Graphics2D g2d) {
        g2d.setColor(node.getFillColor());
        g2d.fillRect(node.getX() - (node.getWidth() / 2), node.getY()
                - (node.getWidth() / 2), node.getWidth(), node.getHeight());
        g2d.setStroke(new BasicStroke(node.getBorderWidth()));
        g2d.setColor(node.getBorderColor());
        g2d.drawRect(node.getX() - (node.getWidth() / 2), node.getY()
                - (node.getWidth() / 2), node.getWidth(), node.getHeight());
    }

    public Point calcIntersectionPoint(final Point end) {
        double width = node.getAbsolutWidth();
        double height = node.getAbsolutHeight();
        double x = node.getX() - width / 2.0;
        double y = node.getY() - height / 2.0;

        DoublePoint vector = new DoublePoint(end.getX() - node.getX(), end
                .getY()
                - node.getY());

        DoublePoint intersection = MathUtils.intersectionOfVectorWithRectangle(
                new Rectangle((int) Math.round(x), (int) Math.round(y),
                        (int) Math.round(width), (int) Math.round(height)),
                vector);

        Point intersectionPoint = null;
        if (intersection != null) {
            intersectionPoint = new Point(
                    (int) Math.round(intersection.getX()), (int) Math
                            .round(intersection.getY()));
        }
        return intersectionPoint;
    }

}
