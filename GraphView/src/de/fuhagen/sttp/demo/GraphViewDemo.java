package de.fuhagen.sttp.demo;

import java.awt.BorderLayout;

import javax.swing.JFrame;
import javax.swing.JLabel;

import de.fuhagen.sttp.data.graph.Node;
import de.fuhagen.sttp.gui.graph.GraphView;
import de.fuhagen.sttp.logic.controller.graph.GraphController;

/**
 * Demo for {@link GraphPanel} and {@link GraphNode}s.
 * 
 * @author thomas
 *
 */
public class GraphViewDemo extends JFrame {

    /**
     * Serial version UID.
     */
    private static final long serialVersionUID = -5247430927670011690L;

    /**
     * This method runs the demo.
     * 
     * @param args
     */
    public static void main(String[] args) {
        new GraphViewDemo();
    }

    /**
     * This constructor builds a new {@link JFrame} which contains a {@link GraphView}.
     */
    public GraphViewDemo() {
        super("Graph view demo");

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLayout(new BorderLayout());

        GraphController controller = new GraphController();
        GraphView graph = controller.createView(true);
        add(graph, BorderLayout.CENTER);

        JLabel hallo = new JLabel("Hallo");
        JLabel welt = new JLabel("Welt");
        graph.add(hallo);
        graph.add(welt);
        hallo.setBounds(70, 90, 5, 5);
        welt.setBounds(210, 280, 5, 5);

        Node a = controller.createNode(200, 100);
        Node b = controller.createNode(100, 200);
        Node c = controller.createNode(500, 300);

        controller.createArc(a, b);
        controller.createArc(b, c);
        controller.createArc(c, a);

        pack();
        setVisible(true);
    }
}
