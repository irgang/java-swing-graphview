package de.fuhagen.sttp.logic.controller.labeledgraph;

import de.fuhagen.sttp.data.graph.Arc;
import de.fuhagen.sttp.data.graph.Node;
import de.fuhagen.sttp.data.labeledgraph.LabeledNode;
import de.fuhagen.sttp.data.labeledgraph.WeightedArc;
import de.fuhagen.sttp.gui.labeledgraph.LabeledGuiNode;
import de.fuhagen.sttp.gui.labeledgraph.WeightedGuiArc;
import de.fuhagen.sttp.logic.controller.graph.GraphController;

public class LabeledGraphController extends GraphController {

    /**
     * This method creates a new labeled graph node on the given
     * position and returns the new node.
     * 
     * @param x
     *      x position of node
     * @param y
     *      y position of node
     * @return
     *      new {@link GraphNode}
     */
    public final LabeledNode createLabeledNode(final int x, final int y,
            String label) {
        LabeledNode node = new LabeledNode(x, y, label);
        addNode(node, new LabeledGuiNode(node));

        //calculate new minimal and maximal point
        invalidate();
        //update all views of this graph
        updateViews();

        return node;
    }

    /**
     * This method creates a new directed {@link Arc} if allowed.
     * 
     * @param start
     *      start node of arc as {@link Node}
     * @param end
     *      end node of arc as {@link Node}
     * @return
     *      new arc as {@link Arc}
     */
    public final WeightedArc createWeightedArc(final Node start,
            final Node end, int weight) {
        WeightedArc arc = null;
        if (canCreateArc(start, end)) {
            arc = new WeightedArc(start, end, weight);
            addArc(arc, new WeightedGuiArc(arc, this));
        }
        return arc;
    }

}
