package de.fuhagen.sttp.gui.graph;

public interface GraphViewListener {

    public void zoomUpdate(double zoom);

    public void translateXUpdate(double x);

    public void translateYUpdate(double y);

}
