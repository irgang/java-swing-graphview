package de.fuhagen.sttp.gui.graph;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.RenderingHints;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.awt.geom.AffineTransform;
import java.util.Vector;

import javax.swing.JComponent;

import de.fuhagen.sttp.data.graph.Node;
import de.fuhagen.sttp.logic.controller.graph.GraphController;
import de.fuhagen.sttp.util.CoordinateTransformations;
import de.fuhagen.sttp.util.DoublePoint;
import de.fuhagen.sttp.util.MathUtils;

/**
 * A {@link GraphView} realize a viewport for a {@link DirectedGraphController}.
 * It shows a visual representation for the graph.
 * 
 * @author thomas
 *
 */
public class GraphView extends JComponent implements MouseListener,
        MouseMotionListener, MouseWheelListener {

    /**
     * Serial version UID.
     */
    private static final long   serialVersionUID  = 1562724783258408340L;

    /**
     * Offset for drawn coordinates of currently moved node.
     */
    private static final int    COORDIANTE_OFFSET = 5;

    /**
     * Size of the large ring of a drag point.
     */
    private static final int    DRAGPOINT_LARGE   = 20;
    /**
     * Soze of the small ring of a drag point.
     */
    private static final int    DRAGPOINT_SMALL   = 10;
    /**
     * Width of the align lines.
     */
    private static final int    ALIGNLINES_WIDTH  = 3;
    /**
     * Length of the dash of align lines.
     */
    private static final int    ALIGNLINE_DASH    = 9;
    /**
     * Size for auto fit views.
     */
    public static final int     AUTOFIT_SIZE      = 100;
    /**
     * Upper zoom limit.
     */
    private static final double UPPER_ZOOM        = 4.0;
    /**
     * Lower zoom limit.
     */
    private static final double LOWER_ZOOM        = 0.25;
    /**
     * Zoom step for mouse wheel.
     */
    private static final double MOUSE_ZOOM_STEP   = 0.1;
    /**
     * Padding of graph for fit in px.
     */
    private static final int    PADDING           = 30;

    /**
     * Available align modes for graph nodes and drag points.
     * 
     * @author thomas
     *
     */
    public enum ALIGN_MODE {
        /**
         * No align of nodes.
         */
        NO_ALIGN,
        /**
         * Align to the fine grid.
         */
        SMALL_ALIGN,
        /**
         * Align to the large grid.
         */
        LARGE_ALIGN
    }

    /**
     * Current align mode of this view. Default is no align.
     */
    private ALIGN_MODE                alignMode           = ALIGN_MODE.NO_ALIGN;

    /**
     * Reference to the controller of this graph.
     */
    private GraphController           controller          = null;

    /**
     * Current zoom factor. Zoom is always equal for both dimensions.
     */
    private double                    zoom;
    /**
     * Translation in vertical direction.
     */
    private double                    translateX;
    /**
     * Translation in horizontal direction.
     */
    private double                    translateY;

    /**
     * True if new arcs should be created. It is necessary to realise this
     * feature as part of the view because we need a visual feedback during
     * arc creation.
     */
    private boolean                   createNewArcs;
    /**
     * Start node of current arc creation.
     */
    private Node                      arcStart;
    /**
     * Current mouse point to draw currently created arc.
     */
    private Point                     arcEnd;

    /**
     * Last drag point. This is required for graph translation.
     */
    private Point                     lastDrag            = null;

    /**
     * If true the graph is automatically zoomed and fit.
     */
    private boolean                   autoFit             = false;

    /**
     * This flag is true if the mouse button is pressed. It is used
     * for drawing the align lines if a node is moved.
     */
    private boolean                   mouseDown           = false;
    /**
     * Listeners of this graph.
     */
    private Vector<GraphViewListener> listeners;

    private boolean                   showHorizontalRuler = true;
    private boolean                   showVerticalRuler   = true;

    /**
     * This constructor create a new {@link GraphView} for the given
     * {@link DirectedGraphController}, which represents the graph. If the
     * mouseSource flag is set it will send user mouse events to the controller.
     * 
     * @param graphControler
     *          {@link DirectedGraphController} of the represented graph.
     * @param mouseSource
     *          If true mouse events are sent to controller.
     */
    public GraphView(final GraphController graphControler,
            final boolean mouseSource) {
        super();

        controller = graphControler;

        listeners = new Vector<GraphViewListener>();

        zoom = 1.0;
        translateX = 0.0;
        translateY = 0.0;

        setSize(controller.getRequiredWidth(), controller.getRequiredHeight());

        if (mouseSource) {
            addMouseListener(this);
            addMouseMotionListener(this);
            addMouseWheelListener(this);
        }

        createNewArcs = false;
        arcStart = null;
        arcEnd = null;
    }

    /**
     * @return the listeners
     */
    public void removeGraphViewListener(GraphViewListener listener) {
        listeners.remove(listener);
    }

    /**
     * @param listeners the listeners to set
     */
    public void addGraphViewListener(GraphViewListener listener) {
        listeners.add(listener);
    }

    /**
     * @return the showHorizontalRuler
     */
    public boolean isHorizontalRuler() {
        return showHorizontalRuler;
    }

    /**
     * @param showHorizontalRuler the showHorizontalRuler to set
     */
    public void showHorizontalRuler(boolean showHorizontalRuler) {
        this.showHorizontalRuler = showHorizontalRuler;
    }

    /**
     * @return the showVerticalRuler
     */
    public boolean isVerticalRuler() {
        return showVerticalRuler;
    }

    /**
     * @param showVerticalRuler the showVerticalRuler to set
     */
    public void showVerticalRuler(boolean showVerticalRuler) {
        this.showVerticalRuler = showVerticalRuler;
    }

    /**
     * This method sets the arc creation mode for this view.
     * 
     * @param newArcFlag
     *            the createNewArcs to set
     */
    public final void setCreateNewArcs(final boolean newArcFlag) {
        createNewArcs = newArcFlag;
    }

    /**
     * This method returns the current new arc creation state of this view.
     * 
     * @return the createNewArcs
     */
    public final boolean isCreateNewArcs() {
        return createNewArcs;
    }

    @Override
    public final void paintComponent(final Graphics g) {
        if (autoFit) {
            fit();
            center();
        }

        Graphics2D g2d = (Graphics2D) g;
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);

        //clear background
        g2d.setColor(Color.WHITE);
        g2d.fillRect(0, 0, getWidth(), getHeight());

        //save old transformation
        AffineTransform oldTransform = g2d.getTransform();
        //do zoom and translate with draw transformation matrix
        AffineTransform transform = new AffineTransform();
        transform.translate(translateX, translateY);
        transform.scale(zoom, zoom);
        transform.concatenate(oldTransform);
        g2d.setTransform(transform);

        //draw all nodes
        for (GuiNode node : controller.getGuiNodes()) {
            node.drawNode(g2d);
        }

        //draw all edges
        for (GuiArc arc : controller.getGuiArcs()) {
            arc.drawArc(g2d);
        }

        //if new arc creation, draw the currently created arc
        if (createNewArcs) {
            if (arcStart != null && arcEnd != null) {
                Point start = controller.getGuiNode(arcStart)
                        .calcIntersectionPoint(arcEnd);
                controller.drawArc(start, arcEnd, g2d, false);
            }
        }

        //if node moved draw aling lines
        if (mouseDown) {
            Node node = controller.getMouseNode();
            if (node != null) {
                drawAlignLines(g2d, new Point(node.getX(), node.getY()));
            }
        }

        //draw selected drag point if there is one
        if (controller.getDragPoint() != null) {
            drawDragPoint(g2d, controller.getDragPoint());
        }

        //draw coordinates of currently moved node
        if (mouseDown && controller.getMouseNode() != null) {
            drawCoords(controller.getMouseNode(), g2d);
        }

        //restore old transformation
        g2d.setTransform(oldTransform);

        //FIXME: ruler, align lines

        double bigdist = (controller.getLargeTic() * zoom);
        double smalldist = (controller.getSmallTic() * zoom);

        if (showHorizontalRuler) {
            //small dashes
            double xpos = (translateX / zoom) % (int) smalldist;
            double ypos = 0;

            g2d.setColor(Color.LIGHT_GRAY);
            g2d.setStroke(new BasicStroke(1));

            while (xpos < getWidth()) {
                g2d.drawLine((int) xpos, (int) ypos, (int) xpos,
                        (int) (ypos + 5));
                xpos += smalldist;
            }

            //big dashes
            xpos = (translateX / zoom) % (int) bigdist;
            ypos = 0;

            g2d.setColor(Color.DARK_GRAY);
            g2d.setStroke(new BasicStroke(3));

            while (xpos < getWidth()) {
                g2d.drawLine((int) xpos, (int) ypos, (int) xpos,
                        (int) (ypos + 10));
                xpos += bigdist;
            }
        }

        if (showVerticalRuler) {
            //small dashes
            double xpos = 0;
            double ypos = (translateY / zoom) % (int) smalldist;

            g2d.setColor(Color.LIGHT_GRAY);
            g2d.setStroke(new BasicStroke(1));

            while (ypos < getHeight()) {
                g2d.drawLine((int) xpos, (int) ypos, (int) (xpos + 5),
                        (int) ypos);
                ypos += smalldist;
            }

            //big dashes
            xpos = 0;
            ypos = (translateY / zoom) % (int) bigdist;

            g2d.setColor(Color.DARK_GRAY);
            g2d.setStroke(new BasicStroke(3));

            while (ypos < getHeight()) {
                g2d.drawLine((int) xpos, (int) ypos, (int) (xpos + 10),
                        (int) ypos);
                ypos += bigdist;
            }
        }
    }

    /**
     * This method draws the coordinates of the currently moved node.
     * 
     * @param node
     *      moved node
     * @param g2d
     *      graphics context
     */
    private void drawCoords(final Node node, final Graphics2D g2d) {
        String text = "(" + node.getX() + "," + node.getY() + ")";

        int x = node.getX() + (node.getWidth() / 2) + COORDIANTE_OFFSET;
        int y = node.getY() + (node.getHeight() / 2) + COORDIANTE_OFFSET;

        Font font = getFont();
        y += getFontMetrics(font).getAscent();

        g2d.drawString(text, x, y);
    }

    /**
     * This method draws the currently selected drag point.
     * 
     * @param g2d
     *      graphics context
     * @param point
     *      position of the drag point
     */
    private void drawDragPoint(final Graphics2D g2d, final Point point) {
        g2d.setColor(controller.getDragPointColor());
        g2d.fillOval(point.x - (DRAGPOINT_LARGE / 2), point.y
                - (DRAGPOINT_LARGE / 2), (DRAGPOINT_LARGE + 1),
                (DRAGPOINT_LARGE + 1));
        g2d.setColor(Color.WHITE);
        g2d.fillOval(point.x - (DRAGPOINT_SMALL / 2), point.y
                - (DRAGPOINT_SMALL / 2), (DRAGPOINT_SMALL + 1),
                (DRAGPOINT_SMALL + 1));
    }

    /**
     * This method draws the align lines for the currently moved node.
     * 
     * @param g2d
     *      graphics context
     * @param center
     *      center point of node
     */
    private void drawAlignLines(final Graphics2D g2d, final Point center) {
        int width = (int) (getWidth() / zoom);
        int height = (int) (getWidth() / zoom);
        int startX = (int) -getTranslateX();
        int startY = (int) -getTranslateY();

        g2d.setColor(Color.BLUE);
        g2d.setStroke(new BasicStroke(ALIGNLINES_WIDTH, BasicStroke.CAP_BUTT,
                BasicStroke.JOIN_BEVEL, 0, new float[] {ALIGNLINE_DASH}, 0));

        g2d.drawLine(startX, center.y, startX + width, center.y);
        g2d.drawLine(center.x, startY, center.x, startY + height);
    }

    /**
     * This method calculates the required size of the graph.
     * 
     * @return
     *      required size as {@link Dimension}
     */
    private Dimension getGraphSize() {
        return new Dimension((int) (zoom * controller.getRequiredWidth() + 1),
                (int) (zoom * controller.getRequiredHeight() + 1));
    }

    @Override
    public final Dimension getPreferredSize() {
        Dimension size = getGraphSize();
        if (autoFit) {
            size = new Dimension(AUTOFIT_SIZE, AUTOFIT_SIZE);
        }
        return size;
    }

    @Override
    public final Dimension getMinimumSize() {
        Dimension size = getGraphSize();
        if (autoFit) {
            size = new Dimension(AUTOFIT_SIZE, AUTOFIT_SIZE);
        }
        return size;
    }

    @Override
    public final Dimension getMaximumSize() {
        return getGraphSize();
    }

    /**
     * This method evaluates the mouse events and forwards it to the controller
     * of the graph represented by this view.
     * 
     * @param e
     *      {@link MouseEvent} e
     */
    @Override
    public final void mouseDragged(final MouseEvent e) {
        Point mousePoint = translateMousePoint(e.getPoint());
        if (createNewArcs) {
            Node node = controller.getNodeAt(mousePoint);
            if (node != null && arcStart == null) {
                arcStart = node;
            } else if (arcStart != null) {
                arcEnd = mousePoint;
                if (node != null) {
                    controller.activeArcCreation(arcStart, node);
                }
            }
            invalidate();
            repaint();
        } else {
            Point alignPoint = new Point(mousePoint.x, mousePoint.y);
            if (alignMode == ALIGN_MODE.SMALL_ALIGN) {
                alignPoint = MathUtils.getAlignPoint(mousePoint, controller
                        .getSmallTic());
            } else if (alignMode == ALIGN_MODE.LARGE_ALIGN) {
                alignPoint = MathUtils.getAlignPoint(mousePoint, controller
                        .getLargeTic());
            }
            boolean translate = controller.mouseDrag(mousePoint, this,
                    alignPoint);
            if (translate) {
                int deltaX = e.getX() - lastDrag.x;
                int deltaY = e.getY() - lastDrag.y;
                setTranslateX(getTranslateX() + deltaX);
                setTranslateY(getTranslateY() + deltaY);
                lastDrag = e.getPoint();
                invalidate();
                repaint();
            }
        }
    }

    /**
     * This method evaluates the mouse events and forwards it to the controller
     * of the graph represented by this view.
     * 
     * @param e
     *      {@link MouseEvent} e
     */
    @Override
    public final void mouseMoved(final MouseEvent e) {
        Point mousePoint = translateMousePoint(e.getPoint());
        if (createNewArcs) {
            if (arcStart != null) {
                arcEnd = mousePoint;
                Node node = controller.getNodeAt(mousePoint);
                if (node != null) {
                    controller.activeArcCreation(arcStart, node);
                }
            }
            invalidate();
            repaint();
        } else {
            Point alignPoint = new Point(mousePoint.x, mousePoint.y);
            if (alignMode == ALIGN_MODE.SMALL_ALIGN) {
                alignPoint = MathUtils.getAlignPoint(mousePoint, controller
                        .getSmallTic());
            } else if (alignMode == ALIGN_MODE.LARGE_ALIGN) {
                alignPoint = MathUtils.getAlignPoint(mousePoint, controller
                        .getLargeTic());
            }
            controller.mousePoint(mousePoint, this, alignPoint);
        }
    }

    /**
     * This method evaluates the mouse events and forwards it to the controller
     * of the graph represented by this view.
     * 
     * @param e
     *      {@link MouseEvent} e
     */
    @Override
    public final void mouseClicked(final MouseEvent e) {
        Point mousePoint = translateMousePoint(e.getPoint());
        if (createNewArcs) {
            Node node = controller.getNodeAt(mousePoint);
            if (arcStart != null && node != null && arcStart != node) {
                controller.createArc(arcStart, node);
                arcStart = null;
                arcEnd = null;
            } else if (arcStart == null && node != null) {
                arcStart = node;
            } else {
                arcStart = null;
                arcEnd = null;
            }
            invalidate();
            repaint();
        } else {
            Point alignPoint = new Point(mousePoint.x, mousePoint.y);
            if (alignMode == ALIGN_MODE.SMALL_ALIGN) {
                alignPoint = MathUtils.getAlignPoint(mousePoint, controller
                        .getSmallTic());
            } else if (alignMode == ALIGN_MODE.LARGE_ALIGN) {
                alignPoint = MathUtils.getAlignPoint(mousePoint, controller
                        .getLargeTic());
            }
            controller.mouseClick(mousePoint, this, alignPoint);
        }
    }

    /**
     * This method evaluates the mouse events and forwards it to the controller
     * of the graph represented by this view.
     * 
     * @param e
     *      {@link MouseEvent} e
     */
    @Override
    public final void mousePressed(final MouseEvent e) {
        lastDrag = e.getPoint();
        mouseDown = true;
    }

    /**
     * This method evaluates the mouse events and forwards it to the controller
     * of the graph represented by this view.
     * 
     * @param e
     *      {@link MouseEvent} e
     */
    @Override
    public final void mouseReleased(final MouseEvent e) {
        lastDrag = null;
        mouseDown = false;

        Point mousePoint = translateMousePoint(e.getPoint());
        if (createNewArcs) {
            Node node = controller.getNodeAt(mousePoint);
            if (arcStart != null && node != null && arcStart != node) {
                controller.createArc(arcStart, node);
                arcStart = null;
                arcEnd = null;
            }
            invalidate();
            repaint();
        }
    }

    /**
     * This method calculates the graph point for a screen point.
     * 
     * @param point
     *      screen point
     * @return
     *      graph point
     */
    private Point translateMousePoint(final Point point) {
        DoublePoint graphPoint = CoordinateTransformations
                .transformScreenPointToGraph(new DoublePoint(translateX,
                        translateY), zoom, zoom, point);
        return new Point((int) graphPoint.getX(), (int) graphPoint.getY());
    }

    /**
     * This method is not used.
     * 
     * @param e
     *      {@link MouseEvent} e
     */
    @Override
    public final void mouseEntered(final MouseEvent e) {
    }

    /**
     * This method is not used.
     * 
     * @param e
     *      {@link MouseEvent} e
     */
    @Override
    public final void mouseExited(final MouseEvent e) {
    }

    /**
     * This method returns the curren zoom factor.
     * 
     * @return the zoom
     */
    public final double getZoom() {
        return zoom;
    }

    /**
     * This method sets the zoom factor.
     * 
     * @param newZoom
     *            the zoom to set
     */
    public final void setZoom(final double newZoom) {
        if (newZoom < LOWER_ZOOM && !autoFit) {
            zoom = LOWER_ZOOM;
        } else if (newZoom > UPPER_ZOOM && !autoFit) {
            zoom = UPPER_ZOOM;
        } else {
            zoom = newZoom;
        }

        for (GraphViewListener listener : listeners) {
            listener.zoomUpdate(zoom);
        }
    }

    /**
     * This method returns the current x translation.
     * 
     * @return the translateX
     */
    public final double getTranslateX() {
        return translateX;
    }

    /**
     * This method sets the x translation.
     * 
     * @param newTranslateX
     *            the translateX to set
     */
    public final void setTranslateX(final double newTranslateX) {
        translateX = newTranslateX;

        for (GraphViewListener listener : listeners) {
            listener.translateXUpdate(translateX);
        }

    }

    /**
     * This method returns the current y translation.
     * 
     * @return the translateY
     */
    public final double getTranslateY() {
        return translateY;
    }

    /**
     * This method sets the current y translation.
     * 
     * @param newTranslateY
     *            the translateY to set
     */
    public final void setTranslateY(final double newTranslateY) {
        translateY = newTranslateY;

        for (GraphViewListener listener : listeners) {
            listener.translateYUpdate(translateY);
        }
    }

    /**
     * This method returns the auto fit state.
     * 
     * @return the autoFit
     */
    public final boolean isAutoFit() {
        return autoFit;
    }

    /**
     * This method sets the auto fit state.
     * 
     * @param autoFitState
     *            the autoFit to set
     */
    public final void setAutoFit(final boolean autoFitState) {
        autoFit = autoFitState;
    }

    /**
     * This method set the zoom such that the graph fits to the 
     * available viewport size and centers the graph into the viewport.
     */
    public final void fit() {
        int width = controller.getRequiredWidth() + (2 * PADDING);
        int height = controller.getRequiredHeight() + (2 * PADDING);

        double zoomX = ((double) getWidth()) / ((double) width);
        double zoomY = ((double) getHeight()) / ((double) height);
        double newZoom = zoomX;
        if (zoomY < newZoom) {
            newZoom = zoomY;
        }
        setZoom(newZoom);
    }

    /**
     * This method centers the graph inside of the viewport.
     */
    public final void center() {

        Point min = controller.getMin();

        int graphWidth = controller.getRequiredWidth() + 2 * PADDING;
        int graphHeight = controller.getRequiredHeight() + 2 * PADDING;

        double realGraphWidth = graphWidth * zoom;
        double realGraphHeight = graphHeight * zoom;

        int offsetX = (int) (getWidth() - realGraphWidth) / 2;
        int offsetY = (int) (getHeight() - realGraphHeight) / 2;

        setTranslateX((-(min.x - PADDING) + offsetX) * zoom);
        setTranslateY((-(min.y - PADDING) + offsetY) * zoom);
    }

    /**
     * This method returns the current align mode of this view.
     * 
     * @return
     *      align mode as {@link ALIGN_MODE}
     */
    public final ALIGN_MODE getAlignMode() {
        return alignMode;
    }

    /**
     * This method sets a new align mode.
     * 
     * @param mode
     *      new align mode as {@link ALIGN_MODE}
     * @param alignAllNodes
     *      if true all graph elements are aligned
     */
    public final void setAlignMode(final ALIGN_MODE mode,
            final boolean alignAllNodes) {
        alignMode = mode;
        if (alignAllNodes) {
            if (mode == ALIGN_MODE.SMALL_ALIGN) {
                controller.alignAllNodes(false);
                controller.alignAllDragPoints(false);
            } else if (mode == ALIGN_MODE.LARGE_ALIGN) {
                controller.alignAllNodes(true);
                controller.alignAllDragPoints(true);
            }
        }
    }

    /**
     * This method change the zoom according to scroll wheel actions.
     * 
     * @param e
     *      {@link MouseWheelEvent}
     */
    @Override
    public final void mouseWheelMoved(final MouseWheelEvent e) {
        double rotation = e.getPreciseWheelRotation();
        rotation = rotation * MOUSE_ZOOM_STEP;
        setZoom(getZoom() + rotation);

        invalidate();
        repaint();
    }

}
