package de.fuhagen.sttp.data.labeledgraph;

import de.fuhagen.sttp.data.graph.Arc;
import de.fuhagen.sttp.data.graph.Node;
import de.fuhagen.sttp.util.DoublePoint;

public class WeightedArc extends Arc {

    private int         weight;
    private DoublePoint offset;

    public WeightedArc(Node startNode, Node endNode, int arcWeight) {
        super(startNode, endNode);

        weight = arcWeight;
        offset = new DoublePoint(10, 10);

    }

    /**
     * @return the offset
     */
    public DoublePoint getOffset() {
        return offset;
    }

    /**
     * @param offset the offset to set
     */
    public void setOffset(DoublePoint offset) {
        this.offset = offset;
    }

    /**
     * @return the weight
     */
    public final int getWeight() {
        return weight;
    }

    /**
     * @param weight the weight to set
     */
    public final void setWeight(final int weight) {
        this.weight = weight;
    }

}
