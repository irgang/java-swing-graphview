package de.fuhagen.sttp.gui.graph;

import java.awt.BasicStroke;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Polygon;
import java.util.Vector;

import de.fuhagen.sttp.data.graph.Arc;
import de.fuhagen.sttp.logic.controller.graph.GraphController;
import de.fuhagen.sttp.util.DoublePoint;
import de.fuhagen.sttp.util.MathUtils;

public class GuiArc {

    /**
     * Full circle.
     */
    private static final double CIRCLE        = 360.0;
    /**
     * Arc width.
     */
    public static final int     ARC_WIDTH     = 5;
    /**
     * Tip angle.
     */
    private static final double TIP_ANGLE     = 20.0;
    /**
     * Radian value of tip angle.
     */
    private static final double TIP_ANGLE_RAD = (2 * Math.PI * TIP_ANGLE)
                                                      / CIRCLE;
    /**
     * Length of tip sides.
     */
    private static final int    TIP_LENGTH    = 20;

    private Arc                 arc;
    private GraphController     controller;

    public GuiArc(Arc data, GraphController gc) {
        super();

        arc = data;
        controller = gc;
    }

    /**
     * This method draws an arc with tip.
     * 
     * @param arc
     *      {@link Arc    } to draw
     * @param g2d
     *      graphics context
     */
    public void drawArc(final Graphics2D g2d) {
        Point start = calcStartPoint(arc);
        Point end = calcEndPoint(arc);

        if (start != null && end != null) {
            g2d.setColor(arc.getColor());

            Vector<Point> dragPoints = new Vector<Point>();
            dragPoints.addAll(arc.getDragPoints());
            dragPoints.insertElementAt(start, 0);
            dragPoints.add(end);

            for (int i = 0; i < dragPoints.size() - 2; i++) {
                drawArc(dragPoints.get(i), dragPoints.get(i + 1), g2d, false);
            }

            drawArc(dragPoints.get(dragPoints.size() - 2), dragPoints
                    .lastElement(), g2d, true);

            drawArcTip(dragPoints.get(dragPoints.size() - 2), dragPoints
                    .lastElement(), g2d);
        }
    }

    /**
     * This method calculates the intersection point of the given arc with the
     * bounds of its start node.
     * 
     * @param arc
     *         arc for calculation
     * @return
     *          intersection point
     */
    protected Point calcStartPoint(final Arc arc) {
        Point end = null;

        if (arc.getDragPoints().isEmpty()) {
            end = new Point(arc.getEnd().getX(), arc.getEnd().getY());
        } else {
            end = new Point(arc.getDragPoints().firstElement().x, arc
                    .getDragPoints().firstElement().y);
        }

        GuiNode startNode = controller.getGuiNode(arc.getStart());
        return startNode.calcIntersectionPoint(end);
    }

    /**
     * This method calculates the intersection point of the given arc
     * with the bounds of its end node.
     * 
     * @param arc
     *          arc as {@link Arc    }
     * @return
     *          intersection point
     */
    protected Point calcEndPoint(final Arc arc) {
        GuiNode endNode = controller.getGuiNode(arc.getEnd());

        Point start = null;
        if (arc.getDragPoints().isEmpty()) {
            start = arc.getStart().getPoint();
        } else {
            start = arc.getDragPoints().lastElement();
        }

        return endNode.calcIntersectionPoint(start);
    }

    /**
     * This method draws a segment of an arc.
     * 
     * @param start
     *      start point
     * @param end
     *      end point
     * @param g2d
     *      graphics context
     * @param lastSegment
     *      flag if it is last segment, which should be a little shorter to not overdraw tip.
     */
    public static void drawArc(final Point start, final Point end,
            final Graphics2D g2d, final boolean lastSegment) {
        g2d.setStroke(new BasicStroke(1));

        if (lastSegment) {
            DoublePoint line = new DoublePoint(start.getX() - end.getX(), start
                    .getY()
                    - end.getY());
            line = MathUtils.sizeVector(line, ARC_WIDTH);

            g2d.drawLine(start.x, start.y, (int) (end.x + line.getX()),
                    (int) (end.y + line.getY()));
        } else {
            g2d.drawLine(start.x, start.y, end.x, end.y);
        }
    }

    /**
     * This method draws the tip of an arc.
     * 
     * @param start
     *      start point of tip
     * @param end
     *      end point of tip
     * @param g2d
     *      graphics context
     */
    public static void drawArcTip(final Point start, final Point end,
            final Graphics2D g2d) {

        DoublePoint vector = new DoublePoint(start.x - end.x, start.y - end.y);

        DoublePoint first = MathUtils.rotateVector(vector, -TIP_ANGLE_RAD);
        first = MathUtils.sizeVector(first, TIP_LENGTH);

        DoublePoint second = MathUtils.rotateVector(vector, TIP_ANGLE_RAD);
        second = MathUtils.sizeVector(second, TIP_LENGTH);

        Point tipTop = new Point(end.x + (int) first.getX(), end.y
                + (int) first.getY());
        Point tipDown = new Point(end.x + (int) second.getX(), end.y
                + (int) second.getY());

        g2d.setStroke(new BasicStroke(1));
        Polygon poly = new Polygon();
        poly.addPoint(end.x, end.y);
        poly.addPoint(tipTop.x, tipTop.y);
        poly.addPoint(tipDown.x, tipDown.y);
        g2d.fillPolygon(poly);
    }

}
