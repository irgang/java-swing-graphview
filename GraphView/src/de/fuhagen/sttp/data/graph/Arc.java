package de.fuhagen.sttp.data.graph;

import java.awt.Color;
import java.awt.Point;
import java.util.Vector;

/**
 * This class represents an arc of a directed graph.
 * 
 * @author thomas
 */
public class Arc {

    /**
     * The default arc color.
     */
    public static final Color ARC_COLOR = Color.GRAY;

    /**
     * The node where this edge starts.
     */
    private Node              start     = null;

    /**
     * The node where this edge ends.
     */
    private Node              end       = null;

    /**
     * The color of this arc.
     */
    private Color             color     = ARC_COLOR;

    /**
     * List of all drag points of this arc.
     */
    private Vector<Point>     dragPoints;

    /**
     * This constructor creates a new edge from startNode to endNode.
     * 
     * @param startNode
     *            node where this edge starts as {@link Node}
     * @param endNode
     *            node where this edge ends as {@link Node}
     */
    public Arc(final Node startNode, final Node endNode) {
        super();

        start = startNode;
        end = endNode;

        dragPoints = new Vector<Point>();
    }

    /**
     * This method creates a new drag point for this arc.
     * 
     * @param pos
     *      position of the drag point
     * @param order
     *      segment number to add the drag point (starting with 0)
     * @return
     *      new drag point as {@link Point}
     */
    public final Point createDragPoint(final Point pos, final int order) {
        Point dragPoint = new Point(pos.x, pos.y);
        dragPoints.insertElementAt(dragPoint, order);
        return dragPoint;
    }

    /**
     * This method deletes the given drag point.
     * 
     * @param dragPoint
     *      drag point as {@link Point}
     */
    public final void deleteDragPoint(final Point dragPoint) {
        dragPoints.remove(dragPoint);
    }

    /**
     * This method returns the list of drag points. DO NOT
     * MODIFY THIS LIST!
     * 
     * @return
     *      list of drag points as {@link Vector} of {@link Point}s
     */
    public final Vector<Point> getDragPoints() {
        return dragPoints;
    }

    /**
     * This method returns the start node of this edge.
     * 
     * @return the start
     */
    public final Node getStart() {
        return start;
    }

    /**
     * This method returns the end node of this edge.
     * 
     * @return the end
     */
    public final Node getEnd() {
        return end;
    }

    /**
     * This method returns the color of this arc.
     * 
     * @return the color
     *      color as {@link Color}
     */
    public final Color getColor() {
        return color;
    }

    /**
     * This method sets the color of this arc.
     * 
     * @param arcColor
     *            the color to set
     */
    public final void setColor(final Color arcColor) {
        color = arcColor;
    }

    /**
     * This method sets the arc color to the default color.
     */
    public final void resetColor() {
        color = ARC_COLOR;
    }

    /**
     * @param start the start to set
     */
    protected void setStart(Node start) {
        this.start = start;
    }

    /**
     * @param end the end to set
     */
    protected void setEnd(Node end) {
        this.end = end;
    }

}
