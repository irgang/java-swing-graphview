package de.fuhagen.sttp.util;

import java.awt.Point;
import java.awt.Rectangle;

/**
 * This class contains math help methods.
 * 
 * @author thomas
 */
public final class MathUtils {

    /**
     * This is a utility class.
     */
    private MathUtils() {

    }

    /**
     * This method resize the given vector as {@link DoublePoint} to the given
     * length.
     * 
     * @param point
     *            vector to resize
     * @param length
     *            new length of the vector
     * @return vector with direction of point and length as given
     */
    public static DoublePoint sizeVector(final DoublePoint point,
            final double length) {
        double sqLen = point.getX() * point.getX() + point.getY()
                * point.getY();
        double oldLength = Math.sqrt(sqLen);
        double fact = length / oldLength;
        return new DoublePoint(fact * point.getX(), fact * point.getY());
    }

    /**
     * This method rotates the vector about the given angle as radian angle.
     * 
     * @param point
     *            vector to rotate
     * @param alpha
     *            angle as radian value
     * @return rotated vector
     */
    public static DoublePoint rotateVector(final DoublePoint point,
            final double alpha) {
        double cosAlpha = Math.cos(alpha);
        double sinAlpha = Math.sin(alpha);

        double[][] rotationMatrix = new double[2][2];
        rotationMatrix[0][0] = cosAlpha;
        rotationMatrix[0][1] = -sinAlpha;
        rotationMatrix[1][0] = sinAlpha;
        rotationMatrix[1][1] = cosAlpha;

        return new DoublePoint(point.getX() * rotationMatrix[0][0]
                + point.getY() * rotationMatrix[0][1], point.getX()
                * rotationMatrix[1][0] + point.getY() * rotationMatrix[1][1]);
    }

    /**
     * This method calculates the intersection point of a rectangle and a
     * vector.
     * 
     * @param rectangle
     *            rectangle as {@link Rectangle}
     * @param vector
     *            vector as {@link DoublePoint}
     * @return intersection point starting from center of the rectangle.
     */
    public static DoublePoint intersectionOfVectorWithRectangle(
            final Rectangle rectangle, final DoublePoint vector) {

        DoublePoint intersection = null;

        if (vector.getX() != 0 || vector.getY() != 0) {

            double yBorder = rectangle.getHeight() / 2.0;
            double xBorder = rectangle.getWidth() / 2.0;

            if (vector.getX() == 0.0) {
                double interX = rectangle.getX() + (rectangle.getWidth() / 2.0);
                if (vector.getY() > 0) {
                    intersection = new DoublePoint(interX, rectangle.getY()
                            + rectangle.getHeight());
                } else {
                    intersection = new DoublePoint(interX, rectangle.getY());
                }
            } else if (vector.getY() == 0.0) {
                if (vector.getX() > 0) {
                    intersection = new DoublePoint(rectangle.getX()
                            + rectangle.getWidth(), rectangle.getY()
                            + rectangle.getHeight() / 2.0);
                } else {
                    intersection = new DoublePoint(rectangle.getX(), rectangle
                            .getY()
                            + rectangle.getHeight() / 2.0);
                }
            } else {
                intersection = intersectionOfVectorWithRectangleNoAxis(
                        rectangle, vector, yBorder, xBorder);
            }
        }
        return intersection;
    }

    /**
     * This method calculates the intersection point of a vector with a rectangle
     * if both vector coordinates are not 0.
     * 
     * @param rectangle
     *            rectangle as {@link Rectangle}
     * @param vector
     *            vector as {@link DoublePoint}
     * @param yBorder
     *            half height of the rectangle
     * @param xBorder
     *            half width of the rectangle
     * @return intersection point starting from center of the rectangle.
     */
    public static DoublePoint intersectionOfVectorWithRectangleNoAxis(
            final Rectangle rectangle, final DoublePoint vector,
            final double yBorder, final double xBorder) {

        DoublePoint intersection = null;

        if (vector.getX() > 0 && vector.getY() > 0) {

            DoublePoint result = intersectionOfVectorWithBounds(xBorder,
                    yBorder, vector);
            intersection = new DoublePoint(rectangle.getCenterX()
                    + result.getX(), rectangle.getCenterY() + result.getY());

        } else if (vector.getX() < 0 && vector.getY() > 0) {

            DoublePoint transformed = new DoublePoint(-vector.getX(), vector
                    .getY());
            DoublePoint result = intersectionOfVectorWithBounds(xBorder,
                    yBorder, transformed);
            intersection = new DoublePoint(rectangle.getCenterX()
                    - result.getX(), rectangle.getCenterY() + result.getY());

        } else if (vector.getX() < 0 && vector.getY() < 0) {

            DoublePoint transformed = new DoublePoint(-vector.getX(), -vector
                    .getY());
            DoublePoint result = intersectionOfVectorWithBounds(xBorder,
                    yBorder, transformed);
            intersection = new DoublePoint(rectangle.getCenterX()
                    - result.getX(), rectangle.getCenterY() - result.getY());

        } else if (vector.getX() > 0 && vector.getY() < 0) {

            DoublePoint transformed = new DoublePoint(vector.getX(), -vector
                    .getY());
            DoublePoint result = intersectionOfVectorWithBounds(xBorder,
                    yBorder, transformed);
            intersection = new DoublePoint(rectangle.getCenterX()
                    + result.getX(), rectangle.getCenterY() - result.getY());
        }

        return intersection;
    }

    /**
     * This method intersects a vector with bounds.
     * 
     * @param xBorder
     *            limit for delta x
     * @param yBorder
     *            limit for delta y
     * @param vector
     *            vector for intersection
     * @return vector to intersection point
     */
    private static DoublePoint intersectionOfVectorWithBounds(
            final double xBorder, final double yBorder, final DoublePoint vector) {
        DoublePoint intersection = null;

        double yOfXIntersect = (vector.getY() / vector.getX()) * xBorder;

        if (yOfXIntersect <= yBorder) {
            intersection = new DoublePoint(xBorder, yOfXIntersect);
        } else {
            double xOfYIntersect = (vector.getX() / vector.getY()) * yBorder;

            intersection = new DoublePoint(xOfYIntersect, yBorder);
        }

        return intersection;
    }

    /**
     * This method calculates the next grid point for a given point and a
     * given grid size.
     * 
     * @param point
     *      point as {@link Point}
     * @param gridSize
     *      grid size as int
     * @return
     *      next grid point
     */
    public static Point getAlignPoint(final Point point, final int gridSize) {
        int x = point.x;
        int y = point.y;

        int delta = x % gridSize;
        if (delta <= gridSize / 2) {
            x = x - delta;
        } else {
            x = x + gridSize - delta;
        }

        delta = y % gridSize;
        if (delta <= gridSize / 2) {
            y = y - delta;
        } else {
            y = y + gridSize - delta;
        }

        return new Point(x, y);
    }
}
