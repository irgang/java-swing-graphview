package de.fuhagen.sttp.data.graph;

import java.awt.Color;
import java.awt.Point;

/**
 * This class represents a node of an directed graph.
 * 
 * @author thomas
 */
public class Node {

    /**
     * This constants defines the size of graph nodes.
     */
    public static final int   NODE_SIZE    = 80;

    /**
     * The default fill color of nodes.
     */
    public static final Color FILL_COLOR   = Color.BLACK;
    /**
     * The default border color of nodes.
     */
    public static final Color BORDER_COLOR = Color.BLACK;
    /**
     * The default border width of nodes.
     */
    public static final int   BORDER_WIDTH = 3;

    /**
     * The x coordinate of this node.
     */
    private int               x            = 0;

    /**
     * The y coordinate of this node.
     */
    private int               y            = 0;

    /**
     * The width of this node.
     */
    private int               width        = NODE_SIZE;

    /**
     * The width of this node.
     */
    private int               height       = NODE_SIZE;

    /**
     * The border color of this node.
     */
    private Color             borderColor;

    /**
     * The fill color of this node.
     */
    private Color             fillColor;

    private int               borderWidth  = BORDER_WIDTH;

    /**
     * This constructor creates a new {@link Node}.
     * 
     * @param centerX
     *      center x coordinate of node
     * @param centerY
     *      center y coordinate of node
     */
    public Node(final int centerX, final int centerY) {
        x = centerX;
        y = centerY;

        borderColor = BORDER_COLOR;
        fillColor = FILL_COLOR;
    }

    /**
     * This method returns the center of this node as point.
     * 
     * @return
     *      center of node as {@link Point}
     */
    public final Point getPoint() {
        return new Point(x, y);
    }

    /**
     * This method returns the border color of this node.
     * 
     * @return the borderColor
     *      border color of this node
     */
    public final Color getBorderColor() {
        return borderColor;
    }

    /**
     * This method sets the border color of this node.
     * 
     * @param newBorderColor
     *            the borderColor to set
     */
    public final void setBorderColor(final Color newBorderColor) {
        borderColor = newBorderColor;
    }

    /**
     * This method sets the border color of this node to the default color.
     */
    public final void resetBorderColor() {
        this.borderColor = BORDER_COLOR;
    }

    /**
     * This method returns the fill color of this node.
     * 
     * @return the fillColor
     *      fill color of this node as {@link Color}
     */
    public final Color getFillColor() {
        return fillColor;
    }

    /**
     * This method sets the fill color of this node.
     * 
     * @param newFillColor
     *            the fillColor to set
     */
    public final void setFillColor(final Color newFillColor) {
        fillColor = newFillColor;
    }

    /**
     * This method returns the center x coordinate of this node.
     * 
     * @return the x
     */
    public final int getX() {
        return x;
    }

    /**
     * This method updates the center x coordinate of this node.
     * 
     * @param xCoordinate
     *            the x coordinate to set
     */
    public final void setX(final int xCoordinate) {
        x = xCoordinate;
    }

    /**
     * This method returns the center y coordinate of this node.
     * 
     * @return the y
     */
    public final int getY() {
        return y;
    }

    /**
     * This method updates the center y coordinate of this node.
     * 
     * @param yCoordinate
     *            the y coordinate to set
     */
    public final void setY(final int yCoordinate) {
        y = yCoordinate;
    }

    /**
     * This method returns the width of this node.
     * 
     * @return the width
     */
    public final int getWidth() {
        return width;
    }

    /**
     * This method returns the height of this node.
     * 
     * @return the height
     */
    public final int getHeight() {
        return height;
    }

    /**
     * This method returns the border width of this node.
     * 
     * @return
     *      border width as int
     */
    public final int getBorderWidth() {
        return borderWidth;
    }

    /**
     * @param borderWidth the borderWidth to set
     */
    protected void setBorderWidth(int borderWidth) {
        this.borderWidth = borderWidth;
    }

    /**
     * This method returns the width of this node including the border width.
     * 
     * @return
     *      absolute width of node as int
     */
    public final int getAbsolutWidth() {
        return width + 2 * borderWidth;
    }

    /**
     * This method returns the height of this node including the border width.
     * 
     * @return
     *      absolute height of node as int
     */
    public final int getAbsolutHeight() {
        return height + 2 * borderWidth;
    }

    /**
     * This method returns an incomplete {@link String} representation of this node.
     * 
     * @return
     *     {@link String} node representation 
     */
    @Override
    public String toString() {
        return "GraphNode at (" + getX() + ", " + getY() + ")";
    }

    /**
     * @param width the width to set
     */
    protected void setWidth(int width) {
        this.width = width;
    }

    /**
     * @param height the height to set
     */
    protected void setHeight(int height) {
        this.height = height;
    }

}
