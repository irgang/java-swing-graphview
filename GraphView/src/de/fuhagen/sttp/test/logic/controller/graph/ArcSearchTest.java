package de.fuhagen.sttp.test.logic.controller.graph;

import static org.junit.Assert.assertTrue;

import java.awt.Point;
import java.util.Vector;

import org.junit.Before;
import org.junit.Test;

import de.fuhagen.sttp.data.graph.Arc;
import de.fuhagen.sttp.data.graph.Node;
import de.fuhagen.sttp.logic.controller.graph.ArcSearch;

/**
 * This class contains the tests for the {@link ArcSearch} class.
 * 
 * @author thomas
 *
 */
public class ArcSearchTest {

    /**
     * Start node of arc, x coordinate.
     */
    private static final int START_NODE_X = -500;
    /**
     * Start node of arc, y coordinate.
     */
    private static final int START_NODE_Y = -500;

    /**
     * End node of arc, x coordinate.
     */
    private static final int END_NODE_X   = 500;
    /**
     * End node of arc, y coordinate.
     */
    private static final int END_NODE_Y   = 500;

    /**
     * Drag point 1 of arc, x coordinate.
     */
    private static final int DRAG_1_X     = 500;
    /**
     * Drag point 1 of arc, y coordinate.
     */
    private static final int DRAG_1_Y     = -500;

    /**
     * Drag point 2 of arc, x coordinate.
     */
    private static final int DRAG_2_X     = -500;
    /**
     * Drag point 2 of arc, y coordinate.
     */
    private static final int DRAG_2_Y     = 500;

    /**
     * No point of arc, x coordinate.
     */
    private static final int NO_X         = 600;
    /**
     * No point of arc, y coordinate.
     */
    private static final int NO_Y         = 600;

    /**
     * Little offset.
     */
    private static final int DELTA        = 2;

    /**
     * {@link Vector} of {@link Arc}s for test.
     */
    private Vector<Arc>      arcs;
    /**
     * Start {@link Node} of test arc. 
     */
    private Node             start;
    /**
     * End {@link Node} of test arc. 
     */
    private Node             end;
    /**
     * {@link Arc} for tests. 
     */
    private Arc              arc;
    /**
     * First drag point of arc.
     */
    private Point            drag1;
    /**
     * Second drag point of arc.
     */
    private Point            drag2;

    /**
     * This method prepares the data for the tests of this class.
     * 
     * @throws Exception
     *      exception if set up of test data fails
     */
    @Before
    public final void setUp() throws Exception {
        arcs = new Vector<Arc>();
        start = new Node(START_NODE_X, START_NODE_Y);
        end = new Node(END_NODE_X, END_NODE_Y);
        arc = new Arc(start, end);
        drag1 = arc.createDragPoint(new Point(DRAG_1_X, DRAG_1_Y), 0);
        drag2 = arc.createDragPoint(new Point(DRAG_2_X, DRAG_2_Y), 1);
        arcs.add(arc);
    }

    /**
     * This method tests if the test arc is found and if null is delivered if
     * no arc is found.
     */
    @Test
    public final void testFindArc() {
        Arc foundArc = ArcSearch.findArc(arcs, new Point(0, 0));
        assertTrue("Find arc.", foundArc == arc);

        foundArc = ArcSearch.findArc(arcs, new Point(NO_X, NO_Y));
        assertTrue("No arc at point.", foundArc == null);
    }

    /**
     * This method tests if drag points are found.
     */
    @Test
    public final void testFindDragPoint() {
        Point dragPoint = ArcSearch.findDragPoint(arc, new Point(DRAG_1_X,
                DRAG_1_Y));
        assertTrue("Drag point is found.", dragPoint == drag1);

        dragPoint = ArcSearch.findDragPoint(arc, new Point(DRAG_2_X + DELTA,
                DRAG_2_Y + DELTA));
        assertTrue("Drag point with delta is found.", dragPoint == drag2);

        dragPoint = ArcSearch.findDragPoint(arc, new Point(NO_X, NO_Y));
        assertTrue("No drag point is found.", dragPoint == null);

    }

    /**
     * This method test the calculation of the drag point order.
     */
    @Test
    public final void testCalcDragPointOrder() {
        int order = ArcSearch.calcDragPointOrder(arc, new Point(0, 0));
        assertTrue("Test if the right arc segment is found.", order == 1);
    }

}
