package de.fuhagen.sttp.util;

public class DoubleRectangle {

    private double x;
    private double y;
    private double width;
    private double height;

    public DoubleRectangle(double xCoordinate, double yCoordinate,
            double width, double height) {
        x = xCoordinate;
        y = yCoordinate;

        this.width = width;
        this.height = height;
    }

    /**
     * @return the x
     */
    public double getX() {
        return x;
    }

    /**
     * @return the y
     */
    public double getY() {
        return y;
    }

    /**
     * @return the width
     */
    public double getWidth() {
        return width;
    }

    /**
     * @return the height
     */
    public double getHeight() {
        return height;
    }

    @Override
    public String toString() {
        return "(" + x + "," + y + ", " + width + ", " + height + ")";
    }

}
