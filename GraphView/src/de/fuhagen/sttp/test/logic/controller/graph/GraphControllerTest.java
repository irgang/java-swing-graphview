package de.fuhagen.sttp.test.logic.controller.graph;

import static org.junit.Assert.assertTrue;

import java.awt.Point;

import org.junit.Before;
import org.junit.Test;

import de.fuhagen.sttp.data.graph.Arc;
import de.fuhagen.sttp.data.graph.Node;
import de.fuhagen.sttp.gui.graph.GraphView;
import de.fuhagen.sttp.logic.controller.graph.GraphController;
import de.fuhagen.sttp.logic.controller.graph.GraphController.MODE;

/**
 * This class contains all test for {@link GraphController}.
 * 
 * @author thomas
 *
 */
public class GraphControllerTest {

    /**
     * X coordinate of test node.
     */
    private static final int TEST_NODE_X     = 100;
    /**
     * Y coordinate of test node.
     */
    private static final int TEST_NODE_Y     = 100;

    /**
     * X coordinate of test node 2.
     */
    private static final int TEST_NODE2_X    = 200;
    /**
     * Y coordinate of test node 2.
     */
    private static final int TEST_NODE2_Y    = 200;

    /**
     * X coordinate of test drag point.
     */
    private static final int TEST_DRAG_X     = 150;
    /**
     * Y coordinate of test drag point.
     */
    private static final int TEST_DRAG_Y     = 400;
    /**
     * Drag delta.
     */
    private static final int TEST_DRAG_DELTA = 10;
    /**
     * Node count after node creation.
     */
    private static final int TEST_NODE_COUNT = 3;

    /**
     * {@link GraphController} instance for tests.
     */
    private GraphController  controller;

    /**
     * Reference to the frist test node.
     */
    private Node             testNode1       = null;

    /**
     * Reference to the second test node.
     */
    private Node             testNode2       = null;

    /**
     * Reference to the drag point.
     */
    private Point            testDrag        = null;

    /**
     * Reference to the test arc.
     */
    private Arc              testArc         = null;

    /**
     * Set up test environment.
     * 
     * @throws Exception
     *      error while setting up
     */
    @Before
    public final void setUp() throws Exception {
        controller = new GraphController();
        testNode1 = controller.createNode(TEST_NODE_X, TEST_NODE_Y);
        testNode2 = controller.createNode(TEST_NODE2_X, TEST_NODE2_Y);
        testArc = controller.createArc(testNode1, testNode2);
        testDrag = testArc.createDragPoint(new Point(TEST_DRAG_X, TEST_DRAG_Y),
                0);
        controller.invalidate();
    }

    /**
     * Test the constructor.
     */
    @Test
    public final void testDirectedGraphController() {
        GraphController graphController = new GraphController();
        assertTrue("No nodes", graphController.getNodes().size() == 0);
        assertTrue("No arcs", graphController.getArcs().size() == 0);
    }

    /**
     * Test if controller can create a view.
     */
    @Test
    public final void testCreateView() {
        GraphView view = controller.createView();
        assertTrue("Controller returns a view.", view != null);
    }

    /**
     * Test for node creation.
     */
    @Test
    public final void testCreateNode() {
        Node node = controller.createNode(TEST_NODE_X, TEST_NODE_Y);
        assertTrue("Controller created node.", node != null);
        assertTrue("Graph contains node.", controller.getNodes().contains(node));
    }

    /**
     * Test for required width calculation.
     */
    @Test
    public final void testGetRequiredWidth() {
        int width = TEST_NODE2_X - TEST_NODE_X;
        width += (testNode1.getAbsolutHeight() / 2);
        width += (testNode2.getAbsolutWidth() / 2);

        assertTrue("Width of graph.", controller.getRequiredWidth() == width);
    }

    /**
     * Test for required height calculation.
     */
    @Test
    public final void testGetRequiredHeight() {
        int height = TEST_DRAG_Y
                - (TEST_NODE_Y - (testNode1.getAbsolutHeight() / 2));

        assertTrue("Height of graph. (" + height + " == "
                + controller.getRequiredHeight() + ")", controller
                .getRequiredHeight() == height);
    }

    /**
     * Test for minimum point of graph.
     */
    @Test
    public final void testGetMin() {
        assertTrue("Minimum point of graph, x.",
                controller.getMin().x == (TEST_NODE_X - (testNode1
                        .getAbsolutWidth() / 2)));
        assertTrue("Minimum point of graph, y.",
                controller.getMin().y == (TEST_NODE_Y - (testNode1
                        .getAbsolutHeight() / 2)));
    }

    /**
     * Test for maximum point of graph.
     */
    @Test
    public final void testGetMax() {
        assertTrue("Maximum point of graph, x.",
                controller.getMax().x == (TEST_NODE2_X + (testNode2
                        .getAbsolutWidth() / 2)));
        assertTrue("Maximum point of graph, y. (" + controller.getMax().y
                + " == " + TEST_DRAG_Y + ")",
                controller.getMax().y == TEST_DRAG_Y);
    }

    /**
     * Test get graph node method.
     */
    @Test
    public final void testGetNodes() {
        assertTrue("Two nodes.", controller.getNodes().size() == 2);
        assertTrue("First node.", controller.getNodes().contains(testNode1));
        assertTrue("Second node.", controller.getNodes().contains(testNode2));
    }

    /**
     * Test get graph arcs method.
     */
    @Test
    public final void testGetArcs() {
        assertTrue("One arc.", controller.getArcs().size() == 1);
        assertTrue("Arc.", controller.getArcs().contains(testArc));
    }

    /**
     * Test creating a new arc.
     */
    @Test
    public final void testCreateArc() {
        Arc arc = controller.createArc(testNode2, testNode1);
        assertTrue("Create new arc.", arc != null);
        assertTrue("Arc is included in list.", controller.getArcs().contains(
                arc));
        arc = controller.createArc(testNode2, testNode1);
        assertTrue("No duplicated arcs.", arc == null);
    }

    /**
     * Test for node highlight if pointed.
     */
    @Test
    public final void testMousePoint() {
        GraphView view = controller.createView();
        Point point = new Point(TEST_NODE_X, TEST_NODE_Y);
        controller.mousePoint(point, view, point);
        assertTrue("Node is highlighted.",
                testNode1.getBorderColor() == GraphController.HIGHLIGHT_COLOR);
    }

    /**
     * Test for drag point highlight color.
     */
    @Test
    public final void testGetDragPointColor() {
        controller.setMode(MODE.MOVE);
        assertTrue(
                "Highlight color.",
                controller.getDragPointColor() == GraphController.HIGHLIGHT_COLOR);
        controller.setMode(MODE.DELETE);
        assertTrue("Delete color. " + controller.getDragPointColor(),
                controller.getDragPointColor().equals(
                        GraphController.DELETE_COLOR));
    }

    /**
     * Test for drag point detection.
     */
    @Test
    public final void testGetDragPoint() {
        GraphView view = controller.createView();
        Point point = new Point(TEST_DRAG_X, TEST_DRAG_Y);
        controller.mousePoint(point, view, point);
        Point drag = controller.getDragPoint();
        assertTrue("Drag found.", drag == testDrag);

        point = new Point(TEST_NODE_X, TEST_NODE_Y);
        controller.mousePoint(point, view, point);
        drag = controller.getDragPoint();
        assertTrue("Drag not found.", drag == null);
    }

    /**
     * Test node creation.
     */
    @Test
    public final void testMouseClick() {
        controller.setMode(MODE.NODE);
        GraphView view = controller.createView();
        Point point = new Point(TEST_NODE_X, TEST_NODE2_Y);
        controller.mouseClick(point, view, point);
        Node node = controller.getNodeAt(point);
        assertTrue("Node created.", node != null);
        assertTrue("Node count.",
                controller.getNodes().size() == TEST_NODE_COUNT);
    }

    /**
     * Test for node drag.
     */
    @Test
    public final void testMouseDrag() {
        controller.setMode(MODE.MOVE);
        GraphView view = controller.createView();
        Point point = new Point(TEST_NODE_X, TEST_NODE_Y);
        controller.mousePoint(point, view, point);
        point = new Point(TEST_NODE_X + TEST_DRAG_DELTA, TEST_NODE_Y
                + TEST_DRAG_DELTA);
        controller.mouseDrag(point, view, point);
        assertTrue("Node moved, x.", testNode1.getX() == TEST_NODE_X
                + TEST_DRAG_DELTA);
        assertTrue("Node moved, y.", testNode1.getY() == TEST_NODE_Y
                + TEST_DRAG_DELTA);
    }

    /**
     * Small tic size is positive.
     */
    @Test
    public final void testGetSmallTic() {
        assertTrue("Tics are positive", controller.getSmallTic() > 0);
    }

    /**
     * Large tic size is positive.
     */
    @Test
    public final void testGetLargeTic() {
        assertTrue("Tics are positive", controller.getLargeTic() > 0);
    }

    /**
     * Pointed node is found.
     */
    @Test
    public final void testGetMouseNode() {
        controller.setMode(MODE.MOVE);
        GraphView view = controller.createView();
        Point point = new Point(TEST_NODE_X, TEST_NODE_Y);
        controller.mousePoint(point, view, point);
        Node node = controller.getMouseNode();
        assertTrue("Node found.", testNode1 == node);
    }

    /**
     * Test if node is found.
     */
    @Test
    public final void testGetNodeAt() {
        Node node = controller.getNodeAt(new Point(TEST_NODE2_X, TEST_NODE2_Y));
        assertTrue("Node found.", node == testNode2);
    }
}
