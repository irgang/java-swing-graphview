package de.fuhagen.sttp.test.util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import de.fuhagen.sttp.util.DoublePoint;
import de.fuhagen.sttp.util.MathUtils;

/**
 * Test cases for {@link MathUtils} methods.
 * 
 * @author thomas
 */
public class MathUtilsTest {

    /**
     *45 Degree.
     */
    private static final double      DEG_45           = 45.0;

    /**
     * 90 Degree.
     */
    private static final double      DEG_90           = 90.0;

    /**
     * 360 Degree.
     */
    private static final double      DEG_360          = 360.0;

    /**
     * Constant for 0.5.
     */
    private static final double      DOTFIVE          = 0.5;

    /**
     * Error tolerance for double operations.
     */
    private static final double      TOLERANCE        = 0.00000001;

    /**
     * 90 degree as radian angle.
     */
    private static final double      RAD90            = 2 * Math.PI * DEG_90
                                                              / DEG_360;
    /**
     * 30 degree as radian angle.
     */
    private static final double      RAD45            = 2 * Math.PI * DEG_45
                                                              / DEG_360;

    /**
     * X coordinate of small vector.
     */
    private static final double      SMALL_COORDINATE = 0.1;

    /**
     * Small vector for tests.
     */
    private static final DoublePoint SMALL            = new DoublePoint(
                                                              SMALL_COORDINATE,
                                                              -SMALL_COORDINATE);
    /**
     * Vector with length 1 for tests.
     */
    private static final DoublePoint ONE              = new DoublePoint(-1, 0);
    /**
     * Large vector for tests.
     */
    private static final DoublePoint LARGE            = new DoublePoint(10000,
                                                              -5000);

    /**
     * Ratio of large vector coordinates.
     */
    private static final int         LARGE_RATIO      = -2;

    /**
     * Test for vector resize with a small vector.
     */
    @Test
    public final void testSizeVectorSmall() {
        DoublePoint result = MathUtils.sizeVector(SMALL, 1);
        double resultLength = Math.sqrt(result.getX() * result.getX()
                + result.getY() * result.getY());
        assertEquals("Length of small vector is 1.", resultLength, 1, TOLERANCE);
        assertTrue("Direction of small vector is same.",
                result.getX() == -result.getY());
    }

    /**
     * Test for vector resize with a vector of length one.
     */
    @Test
    public final void testSizeVectorOne() {
        DoublePoint result = MathUtils.sizeVector(ONE, 1);
        double resultLength = Math.sqrt(result.getX() * result.getX()
                + result.getY() * result.getY());
        assertEquals("Length of one vector is 1.", resultLength, 1, 0.0);
        assertTrue("Direction of one vector is same.", result.getX() == -1
                && result.getY() == 0);

    }

    /**
     * Test for vector resize with a large vector.
     */
    @Test
    public final void testSizeVectorLarge() {
        DoublePoint result = MathUtils.sizeVector(LARGE, DOTFIVE);
        double resultLength = Math.sqrt(result.getX() * result.getX()
                + result.getY() * result.getY());
        assertEquals("Length of large vector is 0.5.", resultLength, DOTFIVE,
                TOLERANCE);
        assertEquals("Direction of large vector is same.", LARGE_RATIO
                * result.getY(), result.getX(), TOLERANCE);
    }

    /**
     * Test for vector rotation with a small vector.
     */
    @Test
    public final void testRotateVectorSmall() {
        DoublePoint result = MathUtils.rotateVector(SMALL, RAD90);
        assertEquals("90° Rotation of small vector.", SMALL_COORDINATE, result
                .getX(), TOLERANCE);
        assertEquals("90° Rotation of small vector.", SMALL_COORDINATE, result
                .getY(), TOLERANCE);

        result = MathUtils.rotateVector(SMALL, RAD45);
        assertEquals("45° Rotation of small vector.", 0, result.getY(),
                TOLERANCE);
    }

    /**
     * Test for vector rotation with a one vector.
     */
    @Test
    public final void testRotateVectorOne() {
        DoublePoint result = MathUtils.rotateVector(ONE, RAD90);
        assertEquals("90° Rotation of one vector.", 0, result.getX(), TOLERANCE);
        assertEquals("90° Rotation of one vector.", -1, result.getY(),
                TOLERANCE);

        result = MathUtils.rotateVector(ONE, RAD45);
        assertEquals("45° Rotation of one vector.", 0, result.getX()
                - result.getY(), TOLERANCE);
    }
}
