package de.fuhagen.sttp.gui.labeledgraph;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;

import de.fuhagen.sttp.data.graph.Node;
import de.fuhagen.sttp.data.labeledgraph.LabeledNode;
import de.fuhagen.sttp.gui.graph.GuiNode;
import de.fuhagen.sttp.util.DoublePoint;

public class LabeledGuiNode extends GuiNode {

    private LabeledNode labeledNode;

    public LabeledGuiNode(LabeledNode data) {
        super(data);
        labeledNode = data;
    }

    /**
     * This method draws a node.
     * 
     * @param node
     *          {@link Node} to draw
     * @param g2d
     *          graphics context
     */
    @Override
    public void drawNode(final Graphics2D g2d) {
        super.drawNode(g2d);

        drawLabel(g2d);
    }

    public void drawLabel(final Graphics2D g2d) {

        String label = labeledNode.getLabel();

        if (label != null) {
            DoublePoint offset = labeledNode.getOffset();
            int x = labeledNode.getX() + (int) offset.getX();
            int y = labeledNode.getY() + (int) offset.getY();

            Font font = g2d.getFont();
            y += g2d.getFontMetrics(font).getAscent();

            int width = g2d.getFontMetrics(font).stringWidth(label);
            x -= width / 2;

            g2d.setColor(Color.GRAY);

            g2d.drawString(label, x, y);
        }
    }
}
