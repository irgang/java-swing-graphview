/**
 * 
 */
package de.fuhagen.sttp.test.gui.graph;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.awt.Dimension;

import org.junit.Before;
import org.junit.Test;

import de.fuhagen.sttp.gui.graph.GraphView;
import de.fuhagen.sttp.gui.graph.GraphView.ALIGN_MODE;
import de.fuhagen.sttp.logic.controller.graph.GraphController;

/**
 * This class contains all tests for {@link GraphView}.
 * 
 * @author thomas
 *
 */
public final class GraphViewTest {

    /**
     * Zoom for set zoom method test.
     */
    private static final double TEST_ZOOM  = 0.567;

    /**
     * Allowed delta for euqality of double values.
     */
    private static final double TEST_DELTA = 0.001;

    /**
     * Graph controller for tests.
     */
    private GraphController     controller;

    /**
     * @throws java.lang.Exception
     *          exceptions while set up test environment
     */
    @Before
    public void setUp() throws Exception {
        controller = new GraphController();
    }

    /**
     * Test for view creation with {@link GraphController}.
     */
    @Test
    public void testGraphView() {
        GraphView view = controller.createView();
        assertFalse("No auto fit by default.", view.isAutoFit());

    }

    /**
     * Test method for arc creation flag.
     */
    @Test
    public void testSetCreateNewArcs() {
        GraphView view = controller.createView();
        view.setCreateNewArcs(true);
        assertTrue("Create new arcs.", view.isCreateNewArcs());
        view.setCreateNewArcs(false);
        assertFalse("Do not create new arcs.", view.isCreateNewArcs());
    }

    /**
     * Test method for auto fit size.
     */
    @Test
    public void testGetPreferredSize() {
        GraphView view = controller.createView();
        view.setAutoFit(true);
        Dimension size = view.getPreferredSize();
        assertTrue("Auto fit width", size.width == GraphView.AUTOFIT_SIZE);
    }

    /**
     * Test method for set zoom method.
     */
    @Test
    public void testSetZoom() {
        GraphView view = controller.createView();
        view.setZoom(TEST_ZOOM);
        assertEquals("Set zoom test.", view.getZoom(), TEST_ZOOM, TEST_DELTA);
    }

    /**
     * Test method for {@link de.fu.vip.view.graph.GraphView#setTranslateX(double)}.
     */
    @Test
    public void testSetTranslateX() {
        GraphView view = controller.createView();
        view.setTranslateX(TEST_ZOOM);
        assertEquals("Set translate x test.", view.getTranslateX(), TEST_ZOOM,
                TEST_DELTA);
    }

    /**
     * Test method for {@link de.fu.vip.view.graph.GraphView#setTranslateY(double)}.
     */
    @Test
    public void testSetTranslateY() {
        GraphView view = controller.createView();
        view.setTranslateY(TEST_ZOOM);
        assertEquals("Set translate y test.", view.getTranslateY(), TEST_ZOOM,
                TEST_DELTA);
    }

    /**
     * Test method for {@link de.fu.vip.view.graph.GraphView#setAutoFit(boolean)}.
     */
    @Test
    public void testSetAutoFit() {
        GraphView view = controller.createView();
        view.setAutoFit(true);
        assertTrue("Set auto fit true.", view.isAutoFit());
        view.setAutoFit(false);
        assertFalse("Set auto fit false.", view.isAutoFit());
    }

    /**
     * Test method for set align mode.
     */
    @Test
    public void testSetAlignMode() {
        GraphView view = controller.createView();
        view.setAlignMode(ALIGN_MODE.NO_ALIGN, false);
        assertTrue("No align.", view.getAlignMode() == ALIGN_MODE.NO_ALIGN);
        view.setAlignMode(ALIGN_MODE.SMALL_ALIGN, false);
        assertTrue("No align.", view.getAlignMode() == ALIGN_MODE.SMALL_ALIGN);
        view.setAlignMode(ALIGN_MODE.LARGE_ALIGN, false);
        assertTrue("No align.", view.getAlignMode() == ALIGN_MODE.LARGE_ALIGN);
    }
}
