package de.fuhagen.sttp.logic.controller.graph;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Point;
import java.util.HashMap;
import java.util.Vector;

import javax.swing.JPanel;

import de.fuhagen.sttp.data.graph.Arc;
import de.fuhagen.sttp.data.graph.Node;
import de.fuhagen.sttp.gui.graph.GraphView;
import de.fuhagen.sttp.gui.graph.GuiArc;
import de.fuhagen.sttp.gui.graph.GuiNode;
import de.fuhagen.sttp.util.MathUtils;

/**
 * This class implements the logic of directed graphs and manage the 
 * directed graph data structure.
 * 
 * @author thomas
 *
 */
public class GraphController {

    /**
     * Color for elements to delete.
     */
    public static final Color DELETE_COLOR     = Color.RED;
    /**
     * Color for pointed elements.
     */
    public static final Color HIGHLIGHT_COLOR  = Color.BLUE;
    /**
     * Highlight color if operation is possible.
     */
    public static final Color CAN_DO_COLOR     = Color.GREEN;
    /**
     * Highlight color if operation is not possible.
     */
    public static final Color CAN_NOT_DO_COLOR = Color.RED;

    /**
     * Size of the small grid.
     */
    private static final int  SMALL_TIC        = 20;
    /**
     * size of the large grid.
     */
    private static final int  LARGE_TIC        = 100;

    /**
     * The x and y coordinate of the minimal max point in 
     * case of an empty graph.
     */
    private static final int  MINIMAL_MAX      = 100;

    /**
     * Modes of the directed graph controller.
     */
    public enum MODE {
        /**
         * Move nodes around with mouse and create drag points.
         */
        MOVE,
        /**
         * Create new nodes with mouse click.
         */
        NODE,
        /**
         * Create new arcs by dragging or with mouse clicks.
         */
        ARC,
        /**
         * Delete nodes, arcs or dragp points with mouse clicks.
         */
        DELETE
    }

    /**
     * All nodes of this graph.
     */
    private Vector<Node>           nodes;

    private HashMap<Node, GuiNode> guiNodes;
    /**
     * All arcs of this graph.
     */
    private Vector<Arc>            arcs;

    private HashMap<Arc, GuiArc>   guiArcs;
    /**
     * Open views of this graph.
     */
    private Vector<GraphView>      views;

    /**
     * Maximal point of this graph.
     */
    private Point                  max;
    /**
     * Minimal point of this graph.
     */

    private Point                  min;

    /**
     * Node which contains the mouse pointer.
     */
    private Node                   mouseNode      = null;
    /**
     * Arc which is next to the mouse pointer.
     */
    private Arc                    mouseArc       = null;
    /**
     * Last mouse point.
     */
    private Point                  mousePoint     = null;
    /**
     * Current pointed drag point.
     */
    private Point                  dragPoint      = null;

    /**
     * Color for drawing drag points.
     */
    private Color                  dragPointColor = HIGHLIGHT_COLOR;

    /**
     * Current operation mode of the controller.
     */
    private MODE                   mode;

    private GraphView              view;
    private GraphView              preview;

    /**
     * This constructor creates a new {@link GraphController} 
     * in MOVE mode.
     */
    public GraphController() {
        nodes = new Vector<Node>();
        guiNodes = new HashMap<Node, GuiNode>();
        arcs = new Vector<Arc>();
        guiArcs = new HashMap<Arc, GuiArc>();

        views = new Vector<GraphView>();

        max = new Point();
        min = new Point();
        mode = MODE.MOVE;

        view = new GraphView(this, true);
        views.add(view);

        preview = new GraphView(this, false);
        preview.setAutoFit(true);
        preview.showHorizontalRuler(false);
        preview.showVerticalRuler(false);
        views.add(preview);
    }

    /**
     * This method creates a new {@link GraphView} for the graph managed
     * from this controller and registers the view for graph updates.
     * 
     * @return
     *      new {@link GraphView}
     */
    public final GraphView createView() {
        return createView(false);
    }

    /**
     * This method creates a new {@link GraphView} for the graph managed
     * from this controller and registers the view for graph updates.
     * 
     * @param mouseSource
     *      true if nodes and graph can be moved by user
     * @return
     *      new {@link GraphView}
     */
    public final GraphView createView(boolean mouseSource) {
        GraphView view = new GraphView(this, mouseSource);
        views.add(view);
        return view;
    }

    /**
     * This method creates a new graph node on the given
     * position and returns the new node.
     * 
     * @param x
     *      x position of node
     * @param y
     *      y position of node
     * @return
     *      new {@link Node}
     */
    public final Node createNode(final int x, final int y) {
        Node node = new Node(x, y);
        addNode(node, new GuiNode(node));

        //calculate new minimal and maximal point
        updateMin();
        updateMax();
        //update all views of this graph
        updateViews();

        return node;
    }

    /**
     * This method calculates the maximal point of this graph.
     */
    private void updateMax() {
        if (nodes.isEmpty()) {
            max.x = MINIMAL_MAX;
            max.y = MINIMAL_MAX;
        } else {
            Node first = nodes.firstElement();
            max.x = first.getX() + first.getAbsolutWidth() / 2;
            max.y = first.getY() + first.getAbsolutHeight() / 2;

            for (Node node : nodes) {
                if (max.x < node.getX() + node.getAbsolutWidth() / 2) {
                    max.x = node.getX() + node.getAbsolutWidth() / 2;
                }

                if (max.y < node.getY() + node.getAbsolutHeight() / 2) {
                    max.y = node.getY() + node.getAbsolutHeight() / 2;
                }
            }
        }

        for (Arc arc : arcs) {
            for (Point drag : arc.getDragPoints()) {
                if (max.x < drag.x) {
                    max.x = drag.x;
                }

                if (max.y < drag.y) {
                    max.y = drag.y;
                }
            }
        }

        //ensure minimal graph size
        if (max.x < min.x + MINIMAL_MAX) {
            max.x = min.x + MINIMAL_MAX;
        }
        if (max.y < min.y + MINIMAL_MAX) {
            max.y = min.y + MINIMAL_MAX;
        }

    }

    /**
     * This method calculates the minimal point of this graph.
     */
    private void updateMin() {
        if (nodes.isEmpty()) {
            min.x = 0;
            min.y = 0;
        } else {
            Node first = nodes.firstElement();
            min.x = first.getX() - first.getAbsolutWidth() / 2;
            min.y = first.getY() - first.getAbsolutHeight() / 2;

            for (Node node : nodes) {
                if (min.x > node.getX() - first.getAbsolutWidth() / 2) {
                    min.x = node.getX() - first.getAbsolutWidth() / 2;
                }

                if (min.y > node.getY() - first.getAbsolutHeight() / 2) {
                    min.y = node.getY() - first.getAbsolutHeight() / 2;
                }
            }
        }

        for (Arc arc : arcs) {
            for (Point drag : arc.getDragPoints()) {
                if (min.x > drag.x) {
                    min.x = drag.x;
                }

                if (min.y > drag.y) {
                    min.y = drag.y;
                }
            }
        }

        //ensure minimal graph size
        if (max.x < min.x + MINIMAL_MAX) {
            max.x = min.x + MINIMAL_MAX;
        }
        if (max.y < min.y + MINIMAL_MAX) {
            max.y = min.y + MINIMAL_MAX;
        }
    }

    /**
     * This method calculates the width of this graph.
     * 
     * @return
     *      width of the graph as int
     */
    public final int getRequiredWidth() {
        return max.x - min.x;
    }

    /**
     * This method calculates the height of this graph.
     * 
     * @return
     *      height of the graph as int
     */
    public final int getRequiredHeight() {
        return max.y - min.y;
    }

    /**
     * This method returns the minimal point of this graph.
     * 
     * @return
     *      minimal point as {@link Point}
     */
    public final Point getMin() {
        return new Point(min.x, min.y);
    }

    /**
     * This method returns the maximal point of this graph.
     * 
     * @return
     *      maximal point as {@link Point}
     */
    public final Point getMax() {
        return new Point(max.x, max.y);
    }

    /**
     * This method triggers the repaint of all views showing this graph.
     */
    protected void updateViews() {
        for (GraphView view : views) {
            view.invalidate();
            view.repaint();
        }
    }

    /**
     * This method returns a reference to the vector containing 
     * all nodes of this graph. DO NOT MODIFY THIS VECTOR!
     * 
     * @return
     *      reference to nodes {@link Vector}
     */
    public final Vector<Node> getNodes() {
        return nodes;
    }

    public final Vector<GuiNode> getGuiNodes() {
        return new Vector<GuiNode>(guiNodes.values());
    }

    public final GuiNode getGuiNode(Node node) {
        return guiNodes.get(node);
    }

    /**
     * This method returns a reference to the vector containing 
     * all arcs of this graph. DO NOT MODIFY THIS VECTOR!
     * 
     * @return
     *      reference to arcs {@link Vector}
     */
    public final Vector<Arc> getArcs() {
        return arcs;
    }

    public final Vector<GuiArc> getGuiArcs() {
        return new Vector<GuiArc>(guiArcs.values());
    }

    /**
     * This method tests if an arc can be created which connects the given
     * start and end node.
     * 
     * A arc can be created if there is no arc and the start and end of the
     * arc are different nodes.
     * 
     * @param start
     *      start node as {@link Node}
     * @param end
     *      end node as {@link Node}
     * @return
     *      true if an arc can be created, false else
     */
    protected boolean canCreateArc(final Node start, final Node end) {
        boolean selfloop = false;

        if (start.equals(end)) {
            selfloop = true;
        }

        boolean exists = false;
        for (Arc arc : arcs) {
            if (arc.getStart() == start && arc.getEnd() == end) {
                exists = true;
                break;
            }
        }

        return !selfloop && !exists;
    }

    /**
     * This method creates a new directed {@link Arc} if allowed.
     * 
     * @param start
     *      start node of arc as {@link Node}
     * @param end
     *      end node of arc as {@link Node}
     * @return
     *      new arc as {@link Arc}
     */
    public final Arc createArc(final Node start, final Node end) {
        Arc arc = null;
        if (canCreateArc(start, end)) {
            arc = new Arc(start, end);
            addArc(arc, new GuiArc(arc, this));
        }
        return arc;
    }

    protected final void addArc(Arc arc, GuiArc guiArc) {
        arcs.add(arc);
        guiArcs.put(arc, guiArc);
    }

    public final GuiArc getGuiArc(Arc arc) {
        return guiArcs.get(arc);
    }

    /**
     * This method is called by graph views if the mouse is moved to a new position.
     * The controller is no mouse listener of the view because each view has its own
     * zoom and translations. The mouse events are used to implement controller actions
     * like creation of new nodes or deletion of elements. Arcs are partially handled 
     * by views because of the visual feedback for arc creation. 
     * 
     * @param point
     *      current mouse point on graph
     * @param view
     *      source view of the mouse event
     * @param alignPoint
     *      current mouse point on graph aligned to grid (or mouse point if no align)
     */
    public final void mousePoint(final Point point, final GraphView view,
            final Point alignPoint) {
        //store current mouse point for drag ...
        mousePoint = point;

        if (mouseNode != null) {
            mouseNode.resetBorderColor();
        }
        if (mouseArc != null) {
            mouseArc.resetColor();
        }

        //search current pointed node
        mouseNode = NodeSearch.findNode(nodes, point);
        if (mouseNode != null) {
            //if a node was found
            if (mode == MODE.DELETE) {
                //color pointed node red if controller is in delete mode
                mouseNode.setBorderColor(DELETE_COLOR);
            } else {
                //color pointed node blue if controller is not in delete mode
                mouseNode.setBorderColor(HIGHLIGHT_COLOR);
            }
            //Reset mouseArc. Nodes have an higher priority than arcs and there can be only one
            //pointed graph element
            mouseArc = null;
            dragPoint = null;
        } else {
            //If no node was found test if an arc is pointed 
            mouseArc = ArcSearch.findArc(arcs, point);
            if (mouseArc != null) {
                //if an arc is pointed
                //test if also a drag point of this arc is pointed
                dragPoint = ArcSearch.findDragPoint(mouseArc, mousePoint);
                if (mode == MODE.DELETE) {
                    //if controller is in delete mode
                    if (dragPoint == null) {
                        //and no drag point is found
                        //color the arc red
                        mouseArc.setColor(DELETE_COLOR);
                    } else {
                        //if a drag point is found color the arc blue
                        mouseArc.setColor(HIGHLIGHT_COLOR);
                    }
                } else {
                    //if not in delete mode
                    //highlight arc
                    mouseArc.setColor(HIGHLIGHT_COLOR);
                }
            } else {
                //if no arc is found
                //reset drag point pointer
                dragPoint = null;
            }
        }
        //repaint all views
        updateViews();
    }

    /**
     * This method returns the current color to paint drag points.
     * Drag points have no own color and the used color can be different
     * of the arc color in delete mode.
     * 
     * @return
     *      Color to paint the current drag point
     */
    public final Color getDragPointColor() {
        return dragPointColor;
    }

    /**
     * This method returns the current pointed drag point or null.
     * 
     * @return
     *      drag point as {@link Point} or null
     */
    public final Point getDragPoint() {
        return dragPoint;
    }

    /**
     * This method is called by graph views if the mouse is clicked.
     * The controller is no mouse listener of the view because each view has its own
     * zoom and translations. The mouse events are used to implement controller actions
     * like creation of new nodes or deletion of elements. Arcs are partially handled 
     * by views because of the visual feedback for arc creation. 
     * 
     * @param point
     *      current mouse point on graph
     * @param view
     *      source view of the mouse event
     * @param alignPoint
     *      current mouse point on graph aligned to grid (or mouse point if no align)
     */
    public final void mouseClick(final Point point, final GraphView view,
            final Point alignPoint) {
        if (mouseNode != null) {
            //if there is a pointed mouse node reset its border color.
            mouseNode.resetBorderColor();
        }
        //search node on click position
        Node node = NodeSearch.findNode(nodes, point);

        if (mode == MODE.DELETE) {
            //if controller is in delete mode
            if (node != null) {
                //and a graph node was found
                //delete this node
                guiNodes.remove(node);
                nodes.remove(node);
                //and search all arcs which start or end with this node
                //if the arcs are deleted while iterating arcs are not tested
                //to avoid this all arcs to delete are collected first.
                Vector<Arc> deleteArcs = new Vector<Arc>();
                for (Arc arc : arcs) {
                    if (arc.getStart() == node) {
                        deleteArcs.add(arc);
                    }
                    if (arc.getEnd() == node) {
                        deleteArcs.add(arc);
                    }
                }
                //and also delete this arcs
                arcs.removeAll(deleteArcs);
                for (Arc arc : deleteArcs) {
                    guiArcs.remove(arc);
                }
                //and reset the mouse node
                mouseNode = null;
            } else {
                //if no node is pointed
                //search arc on click position
                Arc arc = ArcSearch.findArc(arcs, point);
                if (arc != null) {
                    //search for a drag point
                    Point drag = ArcSearch.findDragPoint(arc, point);
                    if (drag != null) {
                        //if a drag point was found
                        //remove this drag point
                        mouseArc.deleteDragPoint(dragPoint);
                    } else {
                        //if no drag point was found
                        //remove the arc
                        arcs.remove(mouseArc);
                        guiArcs.remove(mouseArc);
                    }
                    //reset the mouse arc and drag point
                    mouseArc = null;
                    dragPoint = null;
                }
            }
        } else {
            //if controller is not in delete mode
            if (node != null) {
                //if a node is found set it as mouse node
                mouseNode = node;
                //and highlight the node
                mouseNode.setBorderColor(HIGHLIGHT_COLOR);
            } else if (node == null && mode == MODE.NODE) {
                //if no node was found and controller is in node creation mode
                //create a new node on the aligned click position
                createNode(alignPoint.x, alignPoint.y);
            }
        }
        //repaint all views of this graph
        updateViews();
    }

    /**
     * This method is called by graph views if the mouse is dragged.
     * The controller is no mouse listener of the view because each view has its own
     * zoom and translations. The mouse events are used to implement controller actions
     * like creation of new nodes or deletion of elements. Arcs are partially handled 
     * by views because of the visual feedback for arc creation. 
     * 
     * @param point
     *      current mouse point on graph
     * @param view
     *      source view of the mouse event
     * @param alignPoint
     *      current mouse point on graph aligned to grid (or mouse point if no align)
     * @return
     *      true if the drag should be used for translation of the view area
     */
    public final boolean mouseDrag(final Point point, final GraphView view,
            final Point alignPoint) {
        //do not translate the view area
        boolean translate = false;

        if (mouseNode != null) {
            //if a node was pointed before
            if (mode == MODE.MOVE) {
                //and the controller is in move mode
                //update the nodes position to the aligned coordinates
                mouseNode.setX(alignPoint.x);
                mouseNode.setY(alignPoint.y);
                //and calculate the minimal and maximal point of this graph 
                updateMin();
                updateMax();
                //and repaint all views
                updateViews();
            }
        } else {
            //if no node was pointed before
            if (!(mode == MODE.ARC)) {
                //and controller is not in arc mode
                //use the drag to translate the view area
                translate = true;
            }

            if (mode == MODE.MOVE && mouseArc != null) {
                //if this controller is in move mode and a mouse arc was pointed before
                //do not use this drag for view translation
                translate = false;

                if (dragPoint == null) {
                    //if no drag point was pointed before
                    //create a new drag point
                    dragPoint = mouseArc.createDragPoint(mousePoint, ArcSearch
                            .calcDragPointOrder(mouseArc, mousePoint));
                } else {
                    //if a drag point was pointed before
                    //move this drag point
                    dragPoint.x = alignPoint.x;
                    dragPoint.y = alignPoint.y;
                }
                updateMin();
                updateMax();
                //repaint all views
                updateViews();
            }
        }
        //update the mouse point
        mousePoint = point;
        //tell the view to translate or not
        return translate;
    }

    /**
     * This method returns the size of the small grid.
     * 
     * @return
     *      size of the small grid as int
     */
    public final int getSmallTic() {
        return SMALL_TIC;
    }

    /**
     * This method returns the size of the large grid.
     * 
     * @return
     *      size of the large grid as int
     */
    public final int getLargeTic() {
        return LARGE_TIC;
    }

    /**
     * This method aligns all nodes of this graph to the grid.
     * 
     * @param large
     *      align to large grid
     */
    public final void alignAllNodes(final boolean large) {
        int gridSize = getSmallTic();
        if (large) {
            gridSize = getLargeTic();
        }

        for (Node node : nodes) {
            Point alignPoint = MathUtils.getAlignPoint(new Point(node.getX(),
                    node.getY()), gridSize);
            node.setX(alignPoint.x);
            node.setY(alignPoint.y);
        }
        updateMin();
        updateMax();
        updateViews();
    }

    /**
     * This method aligns all drag points of this graph to the grid.
     * 
     * @param large
     *      if true align to large grid
     */
    public final void alignAllDragPoints(final boolean large) {
        int gridSize = getSmallTic();
        if (large) {
            gridSize = getLargeTic();
        }

        for (Arc arc : arcs) {
            for (Point drag : arc.getDragPoints()) {
                Point alignPoint = MathUtils.getAlignPoint(dragPoint, gridSize);
                drag.x = alignPoint.x;
                drag.y = alignPoint.y;
            }
        }
        updateMin();
        updateMax();
        updateViews();
    }

    /**
     * This method returns the current pointed graph node or null.
     * 
     * @return
     *      {@link Node} or null
     */
    public final Node getMouseNode() {
        return mouseNode;
    }

    /**
     * This method sets a new controller mode. 
     * 
     * @param newMode
     *      new mode as {@link MODE}
     */
    public final void setMode(final MODE newMode) {
        mode = newMode;

        if (mode == MODE.DELETE) {
            dragPointColor = DELETE_COLOR;
        } else {
            dragPointColor = HIGHLIGHT_COLOR;
        }
    }

    /**
     * This method returns the node which contains the given point or null.
     * 
     * @param point
     *      {@link Point}
     * @return
     *      {@link Node} or null
     */
    public final Node getNodeAt(final Point point) {
        return NodeSearch.findNode(nodes, point);
    }

    /**
     * This method is called from views and updates the highlight colors 
     * for nodes during arc creation.
     * 
     * @param startNode
     *      start node of arc
     * @param endNode
     *      end node of arc
     */
    public final void activeArcCreation(final Node startNode, final Node endNode) {
        //reset the old mouse node color
        if (mouseNode != null) {
            mouseNode.resetBorderColor();
        }
        //update mouse node to current end node
        mouseNode = endNode;
        if (canCreateArc(startNode, endNode)) {
            endNode.setBorderColor(CAN_DO_COLOR);
        } else {
            if (startNode == endNode) {
                endNode.setBorderColor(HIGHLIGHT_COLOR);
            } else {
                endNode.setBorderColor(CAN_NOT_DO_COLOR);
            }
        }
        updateViews();
    }

    /**
     * This method "invalidate" the controller and triggers
     * recalculation of the required size.
     */
    public final void invalidate() {
        updateMax();
        updateMin();
    }

    /**
     * This method returns the current mode of this controller.
     * 
     * @return
     *      current mode as {@link MODE}
     */
    public final MODE getMode() {
        return mode;
    }

    protected void addView(GraphView view) {
        views.add(view);
    }

    protected void addNode(Node node, GuiNode guiNode) {
        nodes.add(node);
        guiNodes.put(node, guiNode);
    }

    public void drawArc(final Point start, final Point end,
            final Graphics2D g2d, final boolean lastSegment) {
        GuiArc.drawArc(start, end, g2d, false);
        GuiArc.drawArcTip(start, end, g2d);
    }

    /**
     * @return the view
     */
    public GraphView getView() {
        return view;
    }

    /**
     * @return the preview
     */
    public GraphView getPreview() {
        return preview;
    }

    public void close() {

    }

    public JPanel getGraphTools() {
        return new JPanel();
    }
}
