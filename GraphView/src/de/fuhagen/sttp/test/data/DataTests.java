package de.fuhagen.sttp.test.data;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import de.fuhagen.sttp.test.data.graph.DataGraphTests;
import de.fuhagen.sttp.test.data.labeledgraph.DataLabeledgraphTests;

@RunWith(Suite.class)
@SuiteClasses({DataGraphTests.class, DataLabeledgraphTests.class})
public class DataTests {

}
