package de.fuhagen.sttp.test.logic.controller;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import de.fuhagen.sttp.test.logic.controller.graph.LogicControllerGraphTests;
import de.fuhagen.sttp.test.logic.controller.labeledgraph.LogicControllerLabeledgraphTests;

@RunWith(Suite.class)
@SuiteClasses({LogicControllerGraphTests.class,
        LogicControllerLabeledgraphTests.class})
public class LogicControllerTests {

}
